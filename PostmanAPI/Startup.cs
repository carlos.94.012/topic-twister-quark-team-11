using Google.Cloud.Firestore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PostmanAPI.Services;
using System;
using PostmanAPI.Interfaces.Services;
using PostmanAPI.Repository.GameSessionRepository;
using PostmanAPI.Repository.PlayerRepository;
using PostmanAPI.Repository.RoundRepository;
using PostmanAPI.Repository.TopicRepository;
using PostmanAPI.Repository.TurnRepository;
using PostmanAPI.Repository;

namespace PostmanAPI
{
    public class Startup
    {
        private const string ProjectId = "topic-twister-team11"; 

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string json = "topic-twister-team11-firebase-adminsdk-v6bbc-30f6633168.json";

            string path = AppDomain.CurrentDomain.BaseDirectory + json;
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", path);

            var firestoreDb = FirestoreDb.Create(ProjectId);
            InitRepositories(services, firestoreDb);
            services.AddControllers();

            services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }

        private void InitRepositories(IServiceCollection services, FirestoreDb firestoreDb)
        {
            
            #region PLAYER REPOSITORY
            var playerDao = new FirebasePlayerDAO(firestoreDb);
            var playerRepository = new PlayerRepository(playerDao);
            services.AddTransient(_ => playerRepository);
            services.AddTransient<IPlayerService, PlayerService>(_ => new PlayerService(playerRepository));
            #endregion

            #region GAMESESSION REPOSITORY
            var gameSessionDao = new FirebaseGameSessionDAO(firestoreDb);
            var gameSessionsRepository = new GameSessionRepository(gameSessionDao);
            services.AddTransient<IGameSessionRepository, GameSessionRepository>(_ => gameSessionsRepository);
            services.AddTransient<IGameSessionService, Services.GameSessionService>(_ => new Services.GameSessionService(gameSessionsRepository));
            #endregion
            
            #region ROUND REPOSITORY
            var roundDao = new FirebaseRoundDAO(firestoreDb);
            var roundRepository = new RoundRepository(roundDao);
            services.AddTransient(_ => roundRepository);
            services.AddTransient<IRoundService, RoundService>(_ => new RoundService(roundRepository));
            #endregion

            #region TURN REPOSITORY
            var turnDao = new FirebaseTurnDAO(firestoreDb);
            var turnRepository = new TurnRepository(turnDao);
            services.AddTransient(_ => turnRepository);
            services.AddTransient<ITurnService, TurnService>(_ => new TurnService(turnRepository));
            #endregion

            #region TOPIC REPOSITORY
            var topicDao = new FirebaseTopicDAO(firestoreDb);
            var topicRepository = new TopicRepository(topicDao);
            services.AddTransient(_ => topicRepository);
            services.AddTransient<ITopicService, TopicService>(_ => new TopicService(topicRepository));
            #endregion

            #region ANSWERS REPOSITORY
            var answerDao = new FirebaseAnswerDAO(firestoreDb);
            var answerRepository = new AnswerRepository(answerDao);
            services.AddTransient<IAnswerRepository, AnswerRepository>(_ => answerRepository);
            services.AddTransient<IAnswerService, AnswerService>(_ => new AnswerService(answerRepository));
            #endregion

        }
    }
}
