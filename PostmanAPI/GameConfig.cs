﻿namespace PostmanAPI
{
    public static class GameConfig
    {
        public static int START_ROUND_INDEX = 1;
        public static int MAX_ROUNDS = 3;
        public static int MAX_ANSWER = 5;
        public static int MAX_TOPICS = 5;
    }
}