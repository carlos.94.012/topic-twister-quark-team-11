﻿using PostmanAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.Services;
using PostmanAPI.Repository.TopicRepository;

namespace PostmanAPI.Services
{
    public class TopicService : ITopicService
    {
        private readonly TopicRepository _topicRepository;
        private readonly Random _random = new Random();

        public TopicService(TopicRepository alphabetRepository)
        {
            _topicRepository = alphabetRepository;
        }

        public Task<Topic> GetTopicByName(string name)
        {
            return _topicRepository.GetAsync(name);
        }

        public async Task<IEnumerable<Topic>> LoadRepositoryFromDB()
        {
            return await _topicRepository.GetAllAsync();
        }

        public async Task<IEnumerable<Topic>> GetRandomUniqueTopicsAsync(int count)
        {
            IEnumerable<Topic> topics = await LoadRepositoryFromDB();
            HashSet<Topic> selectedTopics = new HashSet<Topic>();

            while (selectedTopics.Count < count)
            {
                Topic randomTopic = topics.OrderBy(_ => _random.Next()).First();
                if (!selectedTopics.Contains(randomTopic))
                {
                    selectedTopics.Add(randomTopic);
                }
            }

            return selectedTopics;
        }

        public async Task<Dictionary<string, List<string>>> GetWordsByTopic(string[] topics)
        {
            Dictionary<string, List<string>> keyValuePairs = new Dictionary<string, List<string>>();

            foreach (var topic in topics)
            {
                if (!keyValuePairs.ContainsKey(topic))
                {
                    var result = await _topicRepository.GetAsync(topic);
                    keyValuePairs.Add(topic, result.words);
                }
            }

            return keyValuePairs;
        }
    }
}
