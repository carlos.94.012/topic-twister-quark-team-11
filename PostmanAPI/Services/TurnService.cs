﻿using PostmanAPI.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.Services;
using PostmanAPI.Repository;
using PostmanAPI.Repository.TurnRepository;

namespace PostmanAPI.Services
{
    public class TurnService : ITurnService
    {
        private readonly TurnRepository _repository;

        public TurnService(TurnRepository repository)
        {
            _repository = repository;
        }

        public async Task<Turn> CreateTurn(string roundId, string playerId, string[] input)
        {
            var turn = new Turn
            {
                RoundId = roundId,
                Input = input,
                PlayerId = playerId
            };

            await _repository.InsertAsync(turn);
            return turn;
        }

        public async Task<Turn> GetTurn(string roundId, string playerId)
        {
            var queryResult = await _repository.Query(
                new QueryCondition("RoundId", roundId, QueryOperator.EqualTo),
                new QueryCondition("PlayerId", playerId, QueryOperator.EqualTo)
            );

            return queryResult.ElementAtOrDefault(0);
        }

        public async Task<Turn> GetTurnById(string turnId)
        {
            return await _repository.GetAsync(turnId);
        }

        public async Task UpdateTurn(Turn turn)
        {
            await _repository.UpdateAsync(turn);
        }

        public async Task<IEnumerable<Turn>> GetTurnsByRoundId(string roundId)
        {
            return await _repository.Query(
                new QueryCondition("RoundId", roundId, QueryOperator.EqualTo)
            );
        }

        public async Task DeleteAll()
        {
            await _repository.DeleteAll();
        }

        public async Task<IEnumerable<Turn>> Query(params QueryCondition[] conditions)
        {
            return await _repository.Query(conditions);
        }

        public bool[] CheckTurnInputs(Dictionary<string, List<string>> wordsByTopic, string[] inputs, string letter)
        {
            var i = 0;
            var checks = new bool[wordsByTopic.Keys.Count];
            foreach (var key in wordsByTopic.Keys)
            {
                var words = wordsByTopic[key];
                var currentInput = inputs[i];
                checks[i] = IsInputCorrect(currentInput, words, letter);
                i++;
            }

            return checks;
        }

        public int CalculateScore(bool[] checks)
        {
            int score = 0;
            for (int i = 0; i < checks.Length; i++)
            {
                if (checks[i])
                {
                    score++;
                }
            }
            return score;
        }

        public bool IsInputCorrect(string input, List<string> words, string letter)
        {
            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            var charLetter = letter.ToUpper().FirstOrDefault();
            char inputFirstLetter = input.ToUpper().FirstOrDefault();
            bool contains = false;

            for (int i = 0; i < words.Count; i++)
            {
                if (string.Equals(words[i], input, System.StringComparison.OrdinalIgnoreCase))
                {
                    contains = true;
                }
            }

            return inputFirstLetter == charLetter && contains;
        }

        public async Task<Turn> CreateTurn(string roundId, string playerId)
        {
            var turn = new Turn
            {
                RoundId = roundId,
                PlayerId = playerId
            };

            await _repository.InsertAsync(turn);
            return turn;
        }
    }
}
