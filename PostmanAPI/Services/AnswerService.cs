﻿using PostmanAPI.Model;
using PostmanAPI.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostmanAPI.Services
{
    public class AnswerService : IAnswerService
    {
        private readonly IAnswerRepository _repository;

        public AnswerService(IAnswerRepository repository)
        {
            _repository = repository;
        }

        public async Task Create(Answer answer)
        {
            await _repository.InsertAsync(answer);
        }

        public async Task<Answer> Get(string id)
        {
            return await _repository.GetAsync(id);
        }

        public async Task<IEnumerable<Answer>> GetAnswersByTurn(string turnId)
        {
            var firestoreDb = _repository.GetFirestoreDb();
            var query = firestoreDb.Collection("Answers").WhereEqualTo("TurnId", turnId);
            var querySnapshot = await query.GetSnapshotAsync();
            var answers = querySnapshot.Select(doc => doc.ConvertTo<Answer>()).ToList();
            return answers;
        }

        public async Task Update(Answer answer)
        {
            await _repository.UpdateAsync(answer);
        }
    }
}
