﻿using System;

namespace PostmanAPI.Services
{
    public class AlphabetGenerator
    {
        private readonly Random _random = new Random();
        private readonly string _alphabet = "abcdefgijlmnoprstu";

        public char GetRandomLetter(string[] removedLetters)
        {
            // Create a string containing the entire alphabet
            var currentAlphabet = _alphabet;

            // Remove any letters specified in the removedLetters array
            foreach (string letter in removedLetters)
            {
                currentAlphabet = currentAlphabet.Replace(letter, "");
            }

            int randomIndex = _random.Next(0, currentAlphabet.Length);

            // Return the letter at the random index
            return currentAlphabet[randomIndex];
        }
    }
}
