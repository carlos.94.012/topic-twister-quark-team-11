﻿using PostmanAPI.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.Services;
using PostmanAPI.Repository;
using PostmanAPI.Repository.RoundRepository;

namespace PostmanAPI.Services
{
    public class RoundService : IRoundService
    {
        private readonly RoundRepository _repository;

        public RoundService(RoundRepository repository)
        {
            _repository = repository;
        }

        public async Task<Round> CreateRound(string gameSessionId, int roundNumber, char letter, IEnumerable<Topic> topics)
        {
            string[] topicsNames = new string[GameConfig.MAX_TOPICS];

            for (int i = 0; i < GameConfig.MAX_TOPICS; i++)
            {
                topicsNames[i] = topics.ElementAt(i).Name;
            }

            Round round = new Round
            {
                GameSessionId = gameSessionId,
                Letter = letter.ToString(),
                TopicNames = topicsNames.ToArray(),
                RoundNumber = roundNumber
            };

            await _repository.InsertAsync(round);
            return round;
        }

        public async Task<Round> GetRound(string roundId)
        {
            return await _repository.GetAsync(roundId);
        }

        public async Task UpdateRound(Round round)
        {
            await _repository.UpdateAsync(round);
        }

        public async Task<IEnumerable<Round>> Query(params QueryCondition[] conditions)
        {
            return await _repository.Query(conditions);
        }

        public async Task DeleteAll()
        {
            await _repository.DeleteAll();
        }

        public async Task<Round> GetRound(string gameSessionId, int roundNumber)
        {
            var queryResult = await _repository.Query(
                new QueryCondition("RoundNumber", roundNumber, QueryOperator.EqualTo),
                new QueryCondition("GameSessionId", gameSessionId, QueryOperator.EqualTo)
            );

            return queryResult.ElementAtOrDefault(0);
        }
    }
}
