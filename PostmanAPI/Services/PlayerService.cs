﻿using PostmanAPI.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.Services;
using PostmanAPI.Repository.PlayerRepository;

namespace PostmanAPI.Services
{
    public class PlayerService : IPlayerService
    {
        private readonly PlayerRepository _repository;

        public PlayerService(PlayerRepository repository)
        {
            _repository = repository;
        }

        public async Task CreatePlayer(Player player)
        {
            await _repository.InsertAsync(player);
        }

        public async Task<Player> GetPlayer(string id)
        {
            return await _repository.GetAsync(id);
        }

        public async Task UpdatePlayer(Player player)
        {
            await _repository.UpdateAsync(player);
        }

        public async Task DeletePlayer(string id)
        {
            await _repository.DeleteAsync(id);
        }

        public async Task DeleteAll()
        {
            await _repository.DeleteAll();
        }

        public async Task<IEnumerable<Player>> GetAllPlayers()
        {
            return await _repository.GetAllAsync();
        }
    }
}
