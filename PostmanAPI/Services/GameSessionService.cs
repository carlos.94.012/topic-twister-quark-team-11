﻿using PostmanAPI.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.Services;
using PostmanAPI.Repository;
using PostmanAPI.Repository.GameSessionRepository;

namespace PostmanAPI.Services
{
    public class GameSessionService : IGameSessionService
    {
        private readonly GameSessionRepository _repository;

        public GameSessionService(GameSessionRepository gameSessionsRepository)
        {
            _repository = gameSessionsRepository;
        }

        public async Task UpdateGameSession(GameSession gameSession)
        {
            await _repository.UpdateAsync(gameSession);
        }

        public async Task<GameSession> GetGameSessionById(string id)
        {
            return await _repository.GetAsync(id);
        }

        public async Task<IEnumerable<GameSession>> GetGameSessions()
        {
            return await _repository.GetAllAsync();
        }

        public async Task DeleteAll()
        {
            await _repository.DeleteAll();
        }

        public async Task<GameSession> CreateGameSession(string ownerId)
        {
            var gameSession = new GameSession
            {
                OwnerId = ownerId,
                RoundIndex = GameConfig.START_ROUND_INDEX,
            };

            await _repository.InsertAsync(gameSession);

            return gameSession;
        }

        public bool TryGetGameSessionById(string id, out GameSession gameSession)
        {
            gameSession = GetGameSessionById(id).GetAwaiter().GetResult();
            return gameSession != null;
        }

        public async Task<IEnumerable<GameSession>> Query(params QueryCondition[] conditions)
        {
            return await _repository.Query(conditions);
        }

        public bool HasPlayer(GameSession gamesession, string playerId)
        {
            return gamesession.OwnerId == playerId || gamesession.ChallengerId == playerId;
        }

        public bool CheckIsOwner(GameSession gamesession, string playerId)
        {
            return gamesession.OwnerId == playerId;
        }

        public bool PlayerHasCheckedResult(GameSession gamesession, string playerId)
        {
            return CheckIsOwner(gamesession, playerId) ? gamesession.OwnerResultCheck : gamesession.ChallengerResultCheck;
        }
      
        public string GetOpponentId(GameSession gamesession, string playerId)
        {
            return CheckIsOwner(gamesession, playerId) ? gamesession.ChallengerId : gamesession.OwnerId;
        }

        public int GetPlayerScore(GameSession gamesession, string playerId)
        {
            return CheckIsOwner(gamesession, playerId) ? gamesession.OwnerScore : gamesession.ChallengerScore;
        }
    }
}
