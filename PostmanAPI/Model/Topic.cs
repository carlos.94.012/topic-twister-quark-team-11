﻿using Google.Cloud.Firestore;
using System.Collections.Generic;

namespace PostmanAPI.Model
{
    [FirestoreData]
    public class Topic : Entity
    {
        [FirestoreProperty]
        public string Name { get; set; }

        [FirestoreProperty]
        public List<string> words { get; set; }
    }
}
