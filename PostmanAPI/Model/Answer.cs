﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PostmanAPI.Model
{
    [FirestoreData]
    public class Answer : Entity
    {
        [FirestoreProperty]
        public string TopicName { get; set; }

        [FirestoreProperty]
        public string Input { get; set; }

        [FirestoreProperty]
        public bool IsCorrect { get; set; }

        [FirestoreProperty]
        public string TurnId { get; set; }

        public void Validate(List<string> topicWords, string letter)
        {
            if (string.IsNullOrEmpty(Input))
            {
                IsCorrect = false;
                return;
            }

            var charLetter = letter.ToUpper().FirstOrDefault();
            char inputFirstLetter = Input.ToUpper().FirstOrDefault();
            bool contains = false;

            foreach (string topicWord in topicWords)
            {
                if (string.Equals(topicWord, Input, StringComparison.OrdinalIgnoreCase))
                {
                    contains = true;
                }
            }

            IsCorrect = inputFirstLetter == charLetter && contains;
        }
    }
}
