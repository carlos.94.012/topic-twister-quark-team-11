﻿using Google.Cloud.Firestore;
using PostmanAPI.Interfaces;

namespace PostmanAPI.Model
{
    [FirestoreData]
    public class Entity : IEntity
    {
        [FirestoreProperty]
        public string Id { get; set; }
    }
}