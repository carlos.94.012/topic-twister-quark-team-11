﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PostmanAPI.Model
{
    [FirestoreData]
    public class Turn : Entity
    {
        [FirestoreProperty]
        public string PlayerId { get; set; }

        [FirestoreProperty]
        public string RoundId { get; set; }

        [FirestoreProperty]
        public string AnswerId { get; set; }

        [FirestoreProperty]
        public IEnumerable<string> Input { get; set; }

        [FirestoreProperty]
        public IEnumerable<bool> Checks { get; set; }

        [FirestoreProperty]
        public int Score { get; set; }

        public void CalculateScore(bool[] checks)
        {
            Score = checks.Count(c => c);
        }
    }
}
