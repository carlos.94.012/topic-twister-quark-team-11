﻿using Google.Cloud.Firestore;

namespace PostmanAPI.Model
{
    [FirestoreData]
    public class Player : Entity
    {
        [FirestoreProperty]
        public string Name { get; set; }

        [FirestoreProperty]
        public int Coins { get; set; }

        [FirestoreProperty]
        public int Wins { get; set; }

        [FirestoreProperty]
        public bool IsWaitingMatch { get; set; }
    }
}