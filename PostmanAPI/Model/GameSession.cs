﻿using System;
using System.Collections.Generic;

namespace PostmanAPI.Model
{
    public class GameSession : Entity
    {
        public string PlayerWinnerID { get; set; }

        public bool IsClose { get; set; }

        public string ChallengerId { get; set; }

        public string OwnerId { get; set; }

        public int RoundIndex { get; set; }

        public bool IsFinished { get; set; }

        public int OwnerScore { get; set; }

        public int ChallengerScore { get; set; }

        public bool OwnerResultCheck { get;  set; }

        public bool ChallengerResultCheck { get; set; }

        public List<string> Letters { get; set; } = new List<string>();
    }
}

