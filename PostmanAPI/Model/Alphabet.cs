﻿using Google.Cloud.Firestore;

namespace PostmanAPI.Model
{    
    [FirestoreData]
    public class Alphabet : Entity
    {        
        [FirestoreProperty]
        public string Letters { get; private set; }

        public Alphabet() { }
    }
}
