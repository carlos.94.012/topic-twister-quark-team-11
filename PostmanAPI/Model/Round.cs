﻿using Google.Cloud.Firestore;
using PostmanAPI.Services;

namespace PostmanAPI.Model
{
    [FirestoreData]
    public class Round : Entity
    {
        [FirestoreProperty]
        public string[] TopicNames { get; set; }

        [FirestoreProperty]
        public string Letter { get; set; }

        [FirestoreProperty]
        public string GameSessionId { get; set; }

        [FirestoreProperty]
        public string PlayerWinnerID { get; set; }

        [FirestoreProperty]
        public int RoundNumber { get; set; }

        [FirestoreProperty]
        public bool Finished { get; set; }

        [FirestoreProperty]
        public bool OwnerResultCheck { get;  set; }

        [FirestoreProperty]
        public bool ChallengerResultCheck { get;  set; }

        public Round()
        {
            TopicNames = new string[GameConfig.MAX_TOPICS];
        }
    }
}
