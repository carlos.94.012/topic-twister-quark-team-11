﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class PlayerTurnInputDTO
    {
        public string PlayerId { get; set; }
        public string RoundId { get; set; }
    }
}
