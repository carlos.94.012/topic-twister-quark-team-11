﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class GameSessionChecker
    {
        public string GameSessionId { get; set; }
        public string PlayerCheckerId { get; set; }
    }
}


