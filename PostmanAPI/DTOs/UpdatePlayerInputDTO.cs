﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class UpdatePlayerInputDTO 
    {
        public string PlayerId { get; set; }
        public string Name { get; set; }
        public int Coins { get; set; }
        public bool IsWaitingMatch { get; set; }
    }
}