﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class PlayerTurnIdOuputDTO
    {
        public string TurnId { get; set; }
    }
}