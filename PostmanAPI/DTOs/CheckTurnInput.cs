﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class CheckTurnInput
    {
        public string TurnId { get; set; }
        public string[] Topics { get; set; }
        public string Letter { get; set; }
    }
}


