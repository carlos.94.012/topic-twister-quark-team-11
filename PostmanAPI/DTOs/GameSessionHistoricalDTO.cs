namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class GameSessionHistoricalDTO
    {
        public string WinnerName { get; set; }
        public string OponentName { get; set; }
    }
}