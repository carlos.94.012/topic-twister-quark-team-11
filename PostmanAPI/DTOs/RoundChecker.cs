﻿namespace PostmanAPI.Controllers
{

    [System.Serializable]
    public class RoundChecker
    {
        public string RoundId { get; set; }
        public string PlayerCheckerId { get; set; }
        public string OwnerId { get; set; }
        public string ChallengerId { get; set; }
    }
}
