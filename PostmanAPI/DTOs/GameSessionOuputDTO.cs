﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class GameSessionOuputDTO
    {
        public string PlayerWinnerID { get; set; }
        public bool IsClose { get; set; }
        public string Player1ID { get; set; }
        public string Player2ID { get; set; }
        public int RoundIndex { get; set; }
        public string CurrentRoundId { get; set; }
        public bool IsFinished { get; set; }
    }
}
