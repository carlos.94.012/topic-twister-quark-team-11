﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class HandleGameSessionWinnerInput
    {
        public string GameSessionId { get; set; }
    }
}