﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class AnswerInput
    {
        public string TopicName { get; set; }
        public string Input { get; set; }
        public string TurnId { get; set; }
    }
}
