﻿using System.Collections.Generic;

namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class RoundResultOutputDTO
    {
        public string RoundWinnerId { get; set; }
        public IEnumerable<string> TopicsNames { get; set; }
        public int OwnerScore { get; set; }
        public int ChallengerScore { get; set; }
        public IEnumerable<string> OnwerInputs { get; set; }
        public IEnumerable<bool> OnwerChecks { get; set; }
        public IEnumerable<string> ChallengerInputs { get; set; }
        public IEnumerable<bool> ChallengerChecks { get; set; }
    }
}
