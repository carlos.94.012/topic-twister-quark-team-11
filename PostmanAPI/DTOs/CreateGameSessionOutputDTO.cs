﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class CreateGameSessionOutputDTO
    {
        public string GameSessionID { get; set; }
        public string OpponentName { get; set; }
    }
}