﻿using System.Collections.Generic;

namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class CheckTurnOutput
    {
        public IEnumerable<string> Input { get; set; }
        public IEnumerable<bool> Checks { get; set; }
        public string TurnId { get; set; }
    }
}
