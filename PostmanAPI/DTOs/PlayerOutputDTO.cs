﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class PlayerOutputDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Coins { get; set; }
        public bool IsWaitingMatch { get; set; }
    }
}