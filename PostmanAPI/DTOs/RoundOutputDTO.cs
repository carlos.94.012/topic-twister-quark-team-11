﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class RoundOutputDTO
    {
        public string[] TopicNames { get; set; }
        public string Letter { get; set; }
        public string PlayerWinnerID { get; set; }
        public string ID { get; set; }
        public int RoundNumber { get; set; }
        public bool IsLastRound { get; set; }
        public bool OwnerResultCheck { get; set; }
        public bool ChallengerResultCheck { get; set; }
    }
}
