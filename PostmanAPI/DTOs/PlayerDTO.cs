﻿using System;
using System.Collections.Generic;

namespace PostmanAPI.DTOs
{
    [Serializable]
    public class PlayerDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Coins { get; set; }
        public List<string> GameSessionsIds { get; set; } = new List<string>();
    }
}
