﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class JoinGameSessionInputDTO
    {
        public string GameSessionId { get; set; }
        public string PlayerId { get; set; }
    }
}