﻿using System.Collections.Generic;

namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class TurnOutput
    {
        public string TurnId { get; set; }
    }
}
