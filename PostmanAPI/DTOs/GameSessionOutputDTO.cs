﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class GameSessionOutputDTO
    {
        public string Id { get; set; }
        public string PlayerWinnerID { get; set; }
        public bool IsClose { get; set; }
        public string OwnerId { get; set; }
        public string ChallengerId { get; set; }
        public int RoundIndex { get; set; }
        public bool IsFinished { get; set; }
    }
}
