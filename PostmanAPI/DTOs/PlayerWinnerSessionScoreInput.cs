﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class PlayerWinnerSessionScoreInput
    {
        public string GameSessionId { get; set; }
        public string PlayerCallerId { get; set; }
    }
}
