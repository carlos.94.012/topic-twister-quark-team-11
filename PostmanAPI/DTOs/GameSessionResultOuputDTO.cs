﻿namespace PostmanAPI.DTOs
{
    [System.Serializable]
    public class GameSessionResultOuputDTO
    {
        public string PlayerWinnerID { get; set; }
    }
}
