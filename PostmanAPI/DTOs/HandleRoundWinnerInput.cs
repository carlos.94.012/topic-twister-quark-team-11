﻿namespace PostmanAPI.DTOs
{

    [System.Serializable]
    public class HandleRoundWinnerInput
    {
        public string RoundId { get; set; }
        public int OwnerScore { get; set; }
        public int ChallengerScore { get; set; }
        public string OwnerId { get; set; }
        public string ChallengerId { get; set; }
    }
}