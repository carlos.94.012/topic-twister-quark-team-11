using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;

namespace PostmanAPI.Interfaces.DataAccessObject
{
    public interface IPlayerDAO
    {
        Task<Player> GetAsync(string id);
        Task InsertAsync(Player entity);
        Task UpdateAsync(Player entity);
        Task DeleteAsync(string id);
        Task<IEnumerable<Player>> GetAllAsync();
        Task DeleteAll();
    }
}