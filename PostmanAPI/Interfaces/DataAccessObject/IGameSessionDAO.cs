using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Repository;

namespace PostmanAPI.Interfaces.DataAccessObject
{
    public interface IGameSessionDAO
    {
        Task<Model.GameSession> GetAsync(string id);
        Task InsertAsync(Model.GameSession entity);
        Task UpdateAsync(Model.GameSession entity);
        Task<IEnumerable<Model.GameSession>> GetAllAsync();
        Task<IEnumerable<Model.GameSession>> Query(params QueryCondition[] conditions);
        Task DeleteAll();
    }
}