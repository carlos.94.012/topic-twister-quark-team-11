using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;
using PostmanAPI.Repository;

namespace PostmanAPI.Interfaces.DataAccessObject
{
    public interface ITurnDAO
    {
        Task<Turn> GetAsync(string id);
        Task InsertAsync(Turn entity);
        Task UpdateAsync(Turn entity);
        Task<IEnumerable<Turn>> Query(params QueryCondition[] conditions);
        Task DeleteAll();
    }
}