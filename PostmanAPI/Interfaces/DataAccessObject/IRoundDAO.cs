using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;
using PostmanAPI.Repository;

namespace PostmanAPI.Interfaces.DataAccessObject
{
    public interface IRoundDAO
    {
        Task<Round> GetAsync(string id);
        Task InsertAsync(Round entity);
        Task UpdateAsync(Round entity);
        Task<IEnumerable<Round>> Query(params QueryCondition[] conditions);
        Task DeleteAll();
    }
}