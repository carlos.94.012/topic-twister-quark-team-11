using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;

namespace PostmanAPI.Interfaces.DataAccessObject
{
    public interface ITopicDAO
    {
        Task<Topic> GetAsync(string id);
        Task<IEnumerable<Topic>> GetAllAsync();
    }
}