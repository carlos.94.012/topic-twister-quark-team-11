﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;

namespace PostmanAPI.Interfaces.Services
{
    public interface ITopicService : IService<Topic>
    {
        Task<IEnumerable<Topic>> LoadRepositoryFromDB();
        Task<IEnumerable<Topic>> GetRandomUniqueTopicsAsync(int count);
        Task<Topic> GetTopicByName(string name);
        Task<Dictionary<string, List<string>>> GetWordsByTopic(string[] topics);
    }
}