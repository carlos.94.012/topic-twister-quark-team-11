﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;

namespace PostmanAPI.Interfaces.Services
{
    public interface IPlayerService : IService<Player>
    {
        Task CreatePlayer(Player player);
        Task<Player> GetPlayer(string id);
        Task UpdatePlayer(Player player);
        Task DeletePlayer(string id);
        Task<IEnumerable<Player>> GetAllPlayers();
        Task DeleteAll();
    }
}