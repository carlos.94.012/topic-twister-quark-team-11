﻿using PostmanAPI.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PostmanAPI.Services
{
    public interface IAnswerService
    {
        Task<IEnumerable<Answer>> GetAnswersByTurn(string turnId);
        Task Create(Answer answerInput);
        Task Update(Answer answer);
    }
}