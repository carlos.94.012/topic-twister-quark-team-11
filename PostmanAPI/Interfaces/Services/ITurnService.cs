﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;
using PostmanAPI.Repository;

namespace PostmanAPI.Interfaces.Services
{
    public interface ITurnService : IService<Turn>
    {
        Task<Turn> CreateTurn(string roundId, string playerId, string[] input);
        Task<Turn> CreateTurn(string roundId, string playerId);
        Task UpdateTurn(Turn turn);
        Task<Turn> GetTurn(string roundId, string playerId);
        Task<Turn> GetTurnById(string turnId);
        Task<IEnumerable<Turn>> GetTurnsByRoundId(string roundId);
        Task DeleteAll();
        Task<IEnumerable<Turn>> Query(params QueryCondition[] conditions);
        bool[] CheckTurnInputs(Dictionary<string, List<string>> wordsByTopic, string[] inputs, string letter);
        int CalculateScore(bool[] checks);
    }
}