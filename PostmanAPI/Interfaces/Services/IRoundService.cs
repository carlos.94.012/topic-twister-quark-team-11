﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;
using PostmanAPI.Repository;

namespace PostmanAPI.Interfaces.Services
{
    public interface IRoundService : IService<Round>
    {
        Task<Round> GetRound(string roundId);
        Task<Round> GetRound(string id, int roundIndex);

        Task<IEnumerable<Round>> Query(params QueryCondition[] conditions);
        Task UpdateRound(Round round);
        Task DeleteAll();
        Task<Round> CreateRound(string gameSessionId, int roundNumber, char letter, IEnumerable<Topic> topics);
    }
}