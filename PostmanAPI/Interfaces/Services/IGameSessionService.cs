﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;
using PostmanAPI.Repository;

namespace PostmanAPI.Interfaces.Services
{
    public interface IGameSessionService : IService<GameSession>
    {
        Task<GameSession> CreateGameSession(string ownerId);
        Task UpdateGameSession(GameSession gameSession);
        Task<GameSession> GetGameSessionById(string id);
        bool TryGetGameSessionById(string id, out GameSession gameSession);
        Task<IEnumerable<GameSession>> GetGameSessions();
        Task DeleteAll();
        Task<IEnumerable<GameSession>> Query(params QueryCondition[] conditions);
        bool HasPlayer(GameSession gamesession, string playerId);
        bool PlayerHasCheckedResult(GameSession gamesession, string playerId);
        string GetOpponentId(GameSession gamesession, string playerId);
        int GetPlayerScore(GameSession gamesession, string playerId);
        bool CheckIsOwner(GameSession gamesession, string playerId);
    }
}
