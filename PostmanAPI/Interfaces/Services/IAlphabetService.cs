﻿using PostmanAPI.Model;

namespace PostmanAPI.Interfaces.Services
{
    public interface IAlphabetService : IService<Alphabet>
    {
        char GetRandomLetter(string[] letters);
    }
}
