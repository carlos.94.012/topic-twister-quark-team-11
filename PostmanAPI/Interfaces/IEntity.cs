﻿namespace PostmanAPI.Interfaces
{
    public interface IEntity
    {
        public string Id { get; set; }
    }
}
