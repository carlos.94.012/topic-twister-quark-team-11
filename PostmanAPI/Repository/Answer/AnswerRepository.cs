﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Model;
using Google.Cloud.Firestore;

namespace PostmanAPI.Repository
{
    public class AnswerRepository : IAnswerRepository
    {
        private readonly IAnswerDAO _dao;

        public AnswerRepository(IAnswerDAO bdUsed)
        {
            _dao = bdUsed;
        }

        public FirestoreDb GetFirestoreDb()
        {
            return _dao.GetFirestoreDb();
        }

        public async Task InsertAsync(Answer answer)
        {
            await _dao.InsertAsync(answer);
        }

        public async Task<Answer> GetAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return default;
            }

            return await _dao.GetAsync(id);
        }

        public async Task UpdateAsync(Answer answer)
        {
            await _dao.UpdateAsync(answer);
        }

        public async Task<IEnumerable<Answer>> GetAllAsync()
        {
            return await _dao.GetAllAsync();
        }
    }
}