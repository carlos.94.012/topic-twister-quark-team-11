﻿using Google.Cloud.Firestore;
using PostmanAPI.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PostmanAPI.Repository
{
    public interface IAnswerRepository
    {
        Task<IEnumerable<Answer>> GetAllAsync();
        Task<Answer> GetAsync(string id);
        FirestoreDb GetFirestoreDb();
        Task InsertAsync(Answer player);
        Task UpdateAsync(Answer player);
    }
}