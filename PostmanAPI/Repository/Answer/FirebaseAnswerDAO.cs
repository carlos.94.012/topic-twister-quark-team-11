﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Cloud.Firestore;
using PostmanAPI.Model;

namespace PostmanAPI.Repository
{
    public class FirebaseAnswerDAO : IAnswerDAO
    {
        private const string NAME_COLLECTION = "Answers";
        private readonly FirestoreDb _db;
        private readonly CollectionReference _collection;

        public FirebaseAnswerDAO(FirestoreDb db)
        {
            _db = db;
            _collection = _db.Collection(NAME_COLLECTION);
        }

        public FirestoreDb GetFirestoreDb()
        {
            return _db;
        }

        public async Task InsertAsync(Answer entity)
        {
            DocumentReference doc;

            if (string.IsNullOrEmpty(entity.Id))
                doc = _collection.Document();
            else
                doc = _collection.Document(entity.Id);

            entity.Id = doc.Id;

            await doc.SetAsync(entity);
        }

        public async Task<Answer> GetAsync(string id)
        {
            var doc = await _collection.Document(id).GetSnapshotAsync();
            return doc.ConvertTo<Answer>();
        }

        public async Task<IEnumerable<Answer>> GetAllAsync()
        {
            var querySnapshot = await _collection.GetSnapshotAsync();
            var entities = new List<Answer>();

            foreach (var doc in querySnapshot.Documents)
            {
                entities.Add(doc.ConvertTo<Answer>());
            }

            return entities;
        }

        public async Task UpdateAsync(Answer answer)
        {
            var id = answer.Id;
            var doc = _collection.Document(id);
            await doc.SetAsync(answer);
        }
    }
}