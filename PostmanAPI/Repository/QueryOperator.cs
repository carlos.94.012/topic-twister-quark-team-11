﻿namespace PostmanAPI.Repository
{
    public enum QueryOperator
    {
        EqualTo,
        GreaterThan,
        GreaterThanOrEqualTo,
        LessThan,
        LessThanOrEqualTo,
        WhereIn
    }
}