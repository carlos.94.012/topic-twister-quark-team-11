using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Cloud.Firestore;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;

namespace PostmanAPI.Repository.PlayerRepository
{
    public class FirebasePlayerDAO : IPlayerDAO
    {
        private const int LIMIT = 30;
        private const string NAME = "Players";
        private readonly FirestoreDb _db;
        private readonly CollectionReference _collection;

        public FirebasePlayerDAO(FirestoreDb db)
        {
            _db = db;
            _collection = _db.Collection(NAME);
        }

        public async Task<Player> GetAsync(string id)
        {
            var doc = await _collection.Document(id).GetSnapshotAsync();
            return doc.ConvertTo<Player>();
        }

        public async Task InsertAsync(Player entity)
        {
            DocumentReference doc;

            if (string.IsNullOrEmpty(entity.Id))
                doc = _collection.Document();
            else
                doc = _collection.Document(entity.Id);

            entity.Id = doc.Id;

            await doc.SetAsync(entity);
        }

        public async Task UpdateAsync(Player entity)
        {
            var id = GetId(entity);
            var doc = _collection.Document(id);
            await doc.SetAsync(entity);
        }

        public async Task DeleteAsync(string id)
        {
            var doc = _collection.Document(id);
            await doc.DeleteAsync();
        }

        public async Task DeleteAll()
        {
            var querySnapshot = await _collection.GetSnapshotAsync();

            foreach (var doc in querySnapshot.Documents)
            {
                await doc.Reference.DeleteAsync();
            }
        }

        private string GetId(Player entity)
        {
            var idProperty = Array.Find(entity.GetType().GetProperties(), p => p.Name == "Id");
            if (idProperty == null)
            {
                throw new ArgumentException("La entidad no tiene una propiedad 'Id'");
            }

            object id = idProperty.GetValue(entity);

            return (string)id;
        }

        public async Task<IEnumerable<Player>> GetAllAsync()
        {
            var querySnapshot = await _collection.GetSnapshotAsync();
            var entities = new List<Player>();

            foreach (var doc in querySnapshot.Documents)
            {
                entities.Add(doc.ConvertTo<Player>());
            }

            return entities;
        }
    }
}