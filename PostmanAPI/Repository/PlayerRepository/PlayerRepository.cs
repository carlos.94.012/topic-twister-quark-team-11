using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;

namespace PostmanAPI.Repository.PlayerRepository
{
    public class PlayerRepository
    {
        private readonly IPlayerDAO _dao;
        
        public PlayerRepository(IPlayerDAO bdUsed)
        {
            _dao = bdUsed;
        }
        
        public async Task<Player> GetAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return default;
            }

            return await _dao.GetAsync(id);
        }

        public async Task<IEnumerable<Player>> GetAllAsync()
        {
            return await _dao.GetAllAsync();
        }

        public async Task InsertAsync(Player entity)
        {
            await _dao.InsertAsync(entity);
        }

        public async Task UpdateAsync(Player entity)
        {
            await _dao.UpdateAsync(entity);
        }

        public async Task DeleteAsync(string id)
        {
            await _dao.DeleteAsync(id);
        }
        public async Task DeleteAll()
        {
            await _dao.DeleteAll();
        }
    }
}