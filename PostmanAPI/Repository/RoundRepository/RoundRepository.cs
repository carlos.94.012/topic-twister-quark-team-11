using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;

namespace PostmanAPI.Repository.RoundRepository
{
    public class RoundRepository
    {
        private readonly IRoundDAO _dao;
        
        public RoundRepository(IRoundDAO bdUsed)
        {
            _dao = bdUsed;
        }
        
        public async Task<Round> GetAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return default;
            }

            return await _dao.GetAsync(id);
        }

        public async Task InsertAsync(Round entity)
        {
            await _dao.InsertAsync(entity);
        }

        public async Task UpdateAsync(Round entity)
        {
            await _dao.UpdateAsync(entity);
        }

        public async Task DeleteAll()
        {
            await _dao.DeleteAll();
        }

        public async Task<IEnumerable<Round>> Query(params QueryCondition[] conditions)
        {
            return await _dao.Query(conditions);
        }
    }
}