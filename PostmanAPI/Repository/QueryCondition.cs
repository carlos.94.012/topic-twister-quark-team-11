﻿namespace PostmanAPI.Repository
{
    public class QueryCondition
    {
        public string FieldPath;
        public object FieldValue;
        public QueryOperator QueryOperator;

        public QueryCondition(string fieldPath, object fieldValue, QueryOperator queryOperator)
        {
            FieldPath = fieldPath;
            FieldValue = fieldValue;
            QueryOperator = queryOperator;
        }
    }
}