using Google.Cloud.Firestore;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;
using PostmanAPI.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace PostmanAPI.Repository.GameSessionRepository
{
    public class FirebaseGameSessionDAO : IGameSessionDAO
    {
        private const int LIMIT = 30;
        private const string NAME = "GameSessions";
        private readonly FirestoreDb _db;
        private readonly CollectionReference _collection;
        private FirestoreConverter _firestoreConverter = new FirestoreConverter();

        public FirebaseGameSessionDAO(FirestoreDb db)
        {
            _db = db;
            _collection = _db.Collection(NAME);
        }

        public async Task<GameSession> GetAsync(string id)
        {
            var documentReference = _collection.Document(id);
            var documentSnapshot = await documentReference.GetSnapshotAsync();
            var data = documentSnapshot.ToDictionary();
            var gameSession = new GameSession();
            gameSession = _firestoreConverter.Convert(data, gameSession);
            return gameSession;
        }

        //private static void SetProperties(Dictionary<string, object> data, GameSession gameSession)
        //{
        //    foreach (var prop in typeof(GameSession).GetProperties())
        //    {
        //        if (data.TryGetValue(prop.Name, out var value))
        //        {
        //            if (prop.PropertyType == typeof(int))
        //            {
        //                // Cast value to long and then convert to int
        //                value = Convert.ChangeType(value, typeof(long));
        //                value = (int)(long)value;
        //            }
        //            else if (prop.PropertyType == typeof(List<string>))
        //            {
        //                value = ((List<object>)value).ConvertAll(x => x.ToString());
        //            }

        //            prop.SetValue(gameSession, value);
        //        }
        //    }
        

        public async Task InsertAsync(GameSession entity)
        {
            DocumentReference doc = string.IsNullOrEmpty(entity.Id) ? _collection.Document() : _collection.Document(entity.Id);
            entity.Id = doc.Id;
            var data = _firestoreConverter.Convert(entity);
            await doc.SetAsync(data);
        }

        public async Task<IEnumerable<GameSession>> Query(params QueryCondition[] conditions)
        {
            Query query = _collection;

            foreach (var item in conditions)
            {
                switch (item.QueryOperator)
                {
                    case QueryOperator.EqualTo:
                        query = query.WhereEqualTo(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.GreaterThan:
                        query = query.WhereGreaterThan(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.GreaterThanOrEqualTo:
                        query = query.WhereGreaterThanOrEqualTo(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.LessThan:
                        query = query.WhereLessThan(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.LessThanOrEqualTo:
                        query = query.WhereLessThanOrEqualTo(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.WhereIn:
                        var collectionValues = item.FieldValue as IEnumerable;
                        if (collectionValues != null && ((ICollection)collectionValues).Count > LIMIT)
                        {
                            var entities = new List<GameSession>();

                            foreach (var value in collectionValues)
                            {
                                var subQuery = query.WhereEqualTo(item.FieldPath, value);
                                var querySnapshot = await subQuery.GetSnapshotAsync();

                                foreach (var doc in querySnapshot.Documents)
                                {
                                    entities.Add(doc.ConvertTo<GameSession>());
                                }
                            }

                            return entities;
                        }

                        query = query.WhereIn(item.FieldPath, collectionValues);
                        break;
                    default:
                        throw new ArgumentException($"Unsupported operator: {item.QueryOperator}");
                }
            }

            var snapshot = await query.GetSnapshotAsync();
            return snapshot.Documents.Select(doc => doc.ConvertTo<GameSession>());
        }

        public async Task UpdateAsync(GameSession entity)
        {
            var id = GetId(entity);
            var doc = _collection.Document(id);
            var data = _firestoreConverter.Convert(entity);
            await doc.SetAsync(data);
        }

        public async Task DeleteAll()
        {
            var querySnapshot = await _collection.GetSnapshotAsync();

            foreach (var doc in querySnapshot.Documents)
            {
                await doc.Reference.DeleteAsync();
            }
        }

        private string GetId(GameSession entity)
        {
            var idProperty = Array.Find(entity.GetType().GetProperties(), p => p.Name == "Id");
            if (idProperty == null)
            {
                throw new ArgumentException("La entidad no tiene una propiedad 'Id'");
            }

            object id = idProperty.GetValue(entity);

            return (string)id;
        }

        public async Task<IEnumerable<GameSession>> GetAllAsync()
        {
            var querySnapshot = await _collection.GetSnapshotAsync();
            var entities = new List<GameSession>();

            foreach (var doc in querySnapshot.Documents)
            {
                var data = doc.ToDictionary();
                var gameSession = new GameSession();
                gameSession = _firestoreConverter.Convert(data, gameSession);
                entities.Add(gameSession);
            }

            return entities;
        }
    }
}
