using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;

namespace PostmanAPI.Repository.GameSessionRepository
{
    public class GameSessionRepository : IGameSessionRepository
    {
        private readonly IGameSessionDAO _dao;

        public GameSessionRepository(IGameSessionDAO bdUsed)
        {
            _dao = bdUsed;
        }

        public async Task<GameSession> GetAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return default;
            }

            return await _dao.GetAsync(id);
        }

        public async Task<IEnumerable<GameSession>> GetAllAsync()
        {
            return await _dao.GetAllAsync();
        }

        public async Task InsertAsync(GameSession entity)
        {
            await _dao.InsertAsync(entity);
        }

        public async Task UpdateAsync(GameSession entity)
        {
            await _dao.UpdateAsync(entity);
        }

        public async Task DeleteAll()
        {
            await _dao.DeleteAll();
        }

        public async Task<IEnumerable<GameSession>> Query(params QueryCondition[] conditions)
        {
            return await _dao.Query(conditions);
        }
    }
}