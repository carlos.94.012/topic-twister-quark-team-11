﻿using PostmanAPI.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PostmanAPI.Repository.GameSessionRepository
{
    public interface IGameSessionRepository
    {
        Task DeleteAll();
        Task<IEnumerable<GameSession>> GetAllAsync();
        Task<GameSession> GetAsync(string id);
        Task InsertAsync(GameSession entity);
        Task<IEnumerable<GameSession>> Query(params QueryCondition[] conditions);
        Task UpdateAsync(GameSession entity);
    }
}