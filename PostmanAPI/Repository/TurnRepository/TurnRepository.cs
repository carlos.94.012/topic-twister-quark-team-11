using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;

namespace PostmanAPI.Repository.TurnRepository
{
    public class TurnRepository
    {
        private readonly ITurnDAO _dao;

        public TurnRepository(ITurnDAO dao)
        {
            _dao = dao;
        }
        
        public async Task<Turn> GetAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return default;
            }

            return await _dao.GetAsync(id);
        }

        public async Task InsertAsync(Turn entity)
        {
            await _dao.InsertAsync(entity);
        }

        public async Task UpdateAsync(Turn entity)
        {
            await _dao.UpdateAsync(entity);
        }

        public async Task DeleteAll()
        {
            await _dao.DeleteAll();
        }

        public async Task<IEnumerable<Turn>> Query(params QueryCondition[] conditions)
        {
            return await _dao.Query(conditions);
        }
    }
}