using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.Firestore;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;

namespace PostmanAPI.Repository.TurnRepository
{
    public class FirebaseTurnDAO : ITurnDAO
    {
        private const int LIMIT = 30;
        private const string NAME = "Turns";
        private readonly FirestoreDb _db;
        private readonly CollectionReference _collection;

        public FirebaseTurnDAO(FirestoreDb db)
        {
            _db = db;
            _collection = _db.Collection(NAME);
        }

        public async Task<Turn> GetAsync(string id)
        {
            var doc = await _collection.Document(id).GetSnapshotAsync();
            return doc.ConvertTo<Turn>();
        }

        public async Task InsertAsync(Turn entity)
        {
            DocumentReference doc;

            if (string.IsNullOrEmpty(entity.Id))
                doc = _collection.Document();
            else
                doc = _collection.Document(entity.Id);

            entity.Id = doc.Id;

            await doc.SetAsync(entity);
        }

        public async Task<IEnumerable<Turn>> Query(params QueryCondition[] conditions)
        {
            Query query = _collection;

            foreach (var item in conditions)
            {
                switch (item.QueryOperator)
                {
                    case QueryOperator.EqualTo:
                        query = query.WhereEqualTo(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.GreaterThan:
                        query = query.WhereGreaterThan(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.GreaterThanOrEqualTo:
                        query = query.WhereGreaterThanOrEqualTo(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.LessThan:
                        query = query.WhereLessThan(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.LessThanOrEqualTo:
                        query = query.WhereLessThanOrEqualTo(item.FieldPath, item.FieldValue);
                        break;
                    case QueryOperator.WhereIn:
                        var collectionValues = item.FieldValue as IEnumerable;
                        if (collectionValues != null && ((ICollection)collectionValues).Count > LIMIT)
                        {
                            var entities = new List<Turn>();

                            foreach (var value in collectionValues)
                            {
                                var subQuery = query.WhereEqualTo(item.FieldPath, value);
                                var querySnapshot = await subQuery.GetSnapshotAsync();

                                foreach (var doc in querySnapshot.Documents)
                                {
                                    entities.Add(doc.ConvertTo<Turn>());
                                }
                            }

                            return entities;
                        }

                        query = query.WhereIn(item.FieldPath, collectionValues);
                        break;
                    default:
                        throw new ArgumentException($"Unsupported operator: {item.QueryOperator}");
                }
            }

            var snapshot = await query.GetSnapshotAsync();
            return snapshot.Documents.Select(doc => doc.ConvertTo<Turn>());
        }

        public async Task UpdateAsync(Turn entity)
        {
            var id = GetId(entity);
            var doc = _collection.Document(id);
            await doc.SetAsync(entity);
        }

        public async Task DeleteAll()
        {
            var querySnapshot = await _collection.GetSnapshotAsync();

            foreach (var doc in querySnapshot.Documents)
            {
                await doc.Reference.DeleteAsync();
            }
        }

        private string GetId(Turn entity)
        {
            var idProperty = Array.Find(entity.GetType().GetProperties(), p => p.Name == "Id");
            if (idProperty == null)
            {
                throw new ArgumentException("La entidad no tiene una propiedad 'Id'");
            }

            object id = idProperty.GetValue(entity);

            return (string)id;
        }
    }
}