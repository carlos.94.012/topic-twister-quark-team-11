using System.Collections.Generic;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;

namespace PostmanAPI.Repository.TopicRepository
{
    public class TopicRepository
    {
        private readonly ITopicDAO _dao;
        
        public TopicRepository(ITopicDAO bdUsed)
        {
            _dao = bdUsed;
        }
        
        public async Task<Topic> GetAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return default;
            }

            return await _dao.GetAsync(id);
        }

        public async Task<IEnumerable<Topic>> GetAllAsync()
        {
            return await _dao.GetAllAsync();
        }
    }
}