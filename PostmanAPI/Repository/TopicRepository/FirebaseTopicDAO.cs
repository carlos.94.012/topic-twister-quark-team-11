using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Cloud.Firestore;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;

namespace PostmanAPI.Repository.TopicRepository
{
    public class FirebaseTopicDAO : ITopicDAO
    {
        private const string NAME = "Topics";
        private readonly FirestoreDb _db;
        private readonly CollectionReference _collection;
        
        public FirebaseTopicDAO(FirestoreDb db)
        {
            _db = db;
            _collection = _db.Collection(NAME);
        }
        
        public async Task<Topic> GetAsync(string id)
        {
            var doc = await _collection.Document(id).GetSnapshotAsync();
            return doc.ConvertTo<Topic>();
        }

        public async Task<IEnumerable<Topic>> GetAllAsync()
        {
            var querySnapshot = await _collection.GetSnapshotAsync();
            var entities = new List<Topic>();

            foreach (var doc in querySnapshot.Documents)
            {
                entities.Add(doc.ConvertTo<Topic>());
            }

            return entities;
        }
    }
}