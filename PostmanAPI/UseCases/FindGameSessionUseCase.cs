﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PostmanAPI.DTOs;
using PostmanAPI.Interfaces.Services;
using PostmanAPI.Model;
using PostmanAPI.Repository.GameSessionRepository;

public class FindGameSessionUseCase
{
    private const int WIN_TOLERANCE = 5;
    private const int START_ROUND_INDEX = 1;

    private readonly IPlayerService _playerService;
    private readonly Random _random;
    private readonly IGameSessionRepository _gameSessionRepository;
    private readonly IMapper _mapper;

    public FindGameSessionUseCase(IPlayerService userService, IGameSessionRepository gameSessionService, IMapper mapper)
    {
        _playerService = userService;
        _gameSessionRepository = gameSessionService;
        _mapper = mapper;
        _random = new Random();
    }

    public async Task<CreateGameSessionOutputDTO> Find(string playerId)
    {
        var gameSessions = await _gameSessionRepository.GetAllAsync();
        var playerGameSessions = FindPlayerGameSessions(playerId, gameSessions);
        var players = await _playerService.GetAllPlayers();
        var opponents = FindOpponents(playerId, players, playerGameSessions);
        var finder = await _playerService.GetPlayer(playerId);

        if (opponents.Any())
        {
            var match = FindMatchByCriterias(opponents, finder);

            await RemovePlayerFromWaitingList(finder);
            await RemovePlayerFromWaitingList(match);

            var gamesession = await CreateGamesession(finder, match);
            
            var output = _mapper.Map<CreateGameSessionOutputDTO>(gamesession);
            output.OpponentName = match.Name;
            
            return output;
        }

        var openGame = playerGameSessions.FirstOrDefault(x => !x.IsClose);

        if (openGame != null)
        {
            openGame.IsClose = true;
            await _gameSessionRepository.UpdateAsync(openGame);
            var output = _mapper.Map<CreateGameSessionOutputDTO>(openGame);

            if (openGame.OwnerId == finder.Id)
            {
                output.OpponentName = (await _playerService.GetPlayer(openGame.ChallengerId)).Name;
            }
            else if (openGame.ChallengerId == finder.Id)
            {
                output.OpponentName = (await _playerService.GetPlayer(openGame.OwnerId)).Name;
            }

            return output;
        }

        return null;
    }

    private async Task<GameSession> CreateGamesession(Player finder, Player match)
    {
        var newGameSession = new GameSession
        {
            OwnerId = finder.Id,
            ChallengerId = match.Id,
            RoundIndex = START_ROUND_INDEX,
        };

        await _gameSessionRepository.InsertAsync(newGameSession);

        return newGameSession;
    }

    private List<GameSession> FindPlayerGameSessions(string finderId, IEnumerable<GameSession> gameSessions)
    {
        List<GameSession> playerGamesessions = new List<GameSession>();

        foreach (var gameSession in gameSessions)
        {
            if (HasPlayerId(finderId, gameSession))
            {
                playerGamesessions.Add(gameSession);
            }
        }

        return playerGamesessions;
    }

    private bool HasPlayerId(string playerId, GameSession gameSession)
    {
        return gameSession.OwnerId == playerId || gameSession.ChallengerId == playerId;
    }

    private List<Player> FindOpponents(string finderId, IEnumerable<Player> players, IEnumerable<GameSession> gameSessions)
    {
        List<string> opponentsIds = new List<string>();
        foreach (GameSession gamesession in gameSessions)
        {
            if (!gamesession.IsClose)
            {
                if (gamesession.OwnerId == finderId)
                {
                    opponentsIds.Add(gamesession.ChallengerId);
                }
                else
                {
                    opponentsIds.Add(gamesession.OwnerId);
                }
            }
        }

        List<Player> opponents = new List<Player>();
        foreach (var player in players)
        {
            if (player.IsWaitingMatch && player.Id != finderId && !opponentsIds.Contains(player.Id))
            {
                opponents.Add(player);
            }
        }

        return opponents;
    }

    private Player FindMatchByCriterias(List<Player> opponents, Player finder)
    {
        foreach (var opponent in opponents)
        {
            if (IsMatch(opponent, finder))
            {
                return opponent;
            }
        }

        return RandomWaitingPlayer(opponents);
    }

    private bool IsMatch(Player opponent, Player finder)
    {
        return FirstCriteria(opponent, finder) || SecondCriteria(opponent);
    }

    private bool FirstCriteria(Player opponent, Player finder)
    {
        return Math.Abs(opponent.Wins - finder.Wins) <= WIN_TOLERANCE;
    }

    private bool SecondCriteria(Player opponent)
    {
        return opponent.Wins < WIN_TOLERANCE;
    }

    private Player RandomWaitingPlayer(List<Player> opponents)
    {
        var randomPlayerIndex = _random.Next(0, opponents.Count);
        return opponents[randomPlayerIndex];
    }

    private async Task RemovePlayerFromWaitingList(Player player)
    {
        if (player.IsWaitingMatch && player != null)
        {
            player.IsWaitingMatch = false;
            await _playerService.UpdatePlayer(player);
        }
    }
}

