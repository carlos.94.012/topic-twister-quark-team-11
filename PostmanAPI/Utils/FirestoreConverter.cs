﻿using System.Collections.Generic;

namespace PostmanAPI.Utils
{
    public class FirestoreConverter
    {
        public Dictionary<string, object> Convert<T>(T obj)
        {
            var dict = new Dictionary<string, object>();
            foreach (var prop in typeof(T).GetProperties())
            {
                var value = prop.GetValue(obj);
                if (value is long longValue && prop.PropertyType == typeof(int))
                {
                    value = System.Convert.ToInt32(longValue);
                }
                dict[prop.Name] = value;
            }
            return dict;
        }

        public T Convert<T>(Dictionary<string, object> data, T obj)
        {
            foreach (var prop in typeof(T).GetProperties())
            {
                if (data.TryGetValue(prop.Name, out var value))
                {
                    if (prop.PropertyType == typeof(int))
                    {
                        // Cast value to long and then convert to int
                        value = System.Convert.ChangeType(value, typeof(long));
                        value = (int)(long)value;
                    }
                    else if (prop.PropertyType == typeof(List<string>))
                    {
                        value = ((List<object>)value).ConvertAll(x => x.ToString());
                    }

                    prop.SetValue(obj, value);
                }
            }
            return obj;
        }
    }
}