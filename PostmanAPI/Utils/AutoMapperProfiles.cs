﻿using AutoMapper;
using PostmanAPI.DTOs;
using PostmanAPI.Model;

namespace PostmanAPI.Utils
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<GameSession, GameSessionResultOuputDTO>()
                .ForMember(dest => dest.PlayerWinnerID, opt => opt.MapFrom(src => src.PlayerWinnerID));

            CreateMap<Turn, TurnOutput>()
                .ForMember(dest => dest.TurnId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Turn, CheckTurnOutput>()
                .ForMember(dest => dest.Input, opt => opt.MapFrom(src => src.Input))
                .ForMember(dest => dest.Checks, opt => opt.MapFrom(src => src.Checks))
                .ForMember(dest => dest.TurnId, opt => opt.MapFrom(src => src.Id));

            CreateMap<Round, RoundOutputDTO>()
                .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.TopicNames, opt => opt.MapFrom(src => src.TopicNames))
                .ForMember(dest => dest.RoundNumber, opt => opt.MapFrom(src => src.RoundNumber))
                .ForMember(dest => dest.Letter, opt => opt.MapFrom(src => src.Letter))
                .ForMember(dest => dest.PlayerWinnerID, opt => opt.MapFrom(src => src.PlayerWinnerID))
                .ForMember(dest => dest.ChallengerResultCheck, opt => opt.MapFrom(src => src.GameSessionId))
                .ForMember(dest => dest.OwnerResultCheck, opt => opt.MapFrom(src => src.OwnerResultCheck))
                .ForMember(dest => dest.ChallengerResultCheck, opt => opt.MapFrom(src => src.ChallengerResultCheck));

            CreateMap<Round, RoundResultOutputDTO>()
                .ForMember(dest => dest.TopicsNames, opt => opt.MapFrom(src => src.TopicNames))
                .ForMember(dest => dest.OnwerInputs, opt => opt.Ignore())
                .ForMember(dest => dest.OnwerChecks, opt => opt.Ignore())
                .ForMember(dest => dest.ChallengerInputs, opt => opt.Ignore())
                .ForMember(dest => dest.ChallengerChecks, opt => opt.Ignore())
                .ForMember(dest => dest.RoundWinnerId, opt => opt.Ignore());

            CreateMap<Turn, RoundResultOutputDTO>()
                .ForMember(dest => dest.OnwerInputs, opt => opt.MapFrom(src => src.Input))
                .ForMember(dest => dest.OnwerChecks, opt => opt.MapFrom(src => src.Checks))
                .ForMember(dest => dest.ChallengerInputs, opt => opt.MapFrom(src => src.Input))
                .ForMember(dest => dest.ChallengerChecks, opt => opt.MapFrom(src => src.Checks))
                .ForMember(dest => dest.RoundWinnerId, opt => opt.Ignore());

            CreateMap<GameSession, GameSessionOutputDTO>()
               .ForMember(dest => dest.IsClose, opt => opt.MapFrom(src => src.IsClose))
               .ForMember(dest => dest.IsFinished, opt => opt.MapFrom(src => src.IsFinished))
               .ForMember(dest => dest.OwnerId, opt => opt.MapFrom(src => src.OwnerId))
               .ForMember(dest => dest.ChallengerId, opt => opt.MapFrom(src => src.ChallengerId))
               .ForMember(dest => dest.PlayerWinnerID, opt => opt.MapFrom(src => src.PlayerWinnerID))
               .ForMember(dest => dest.RoundIndex, opt => opt.MapFrom(src => src.RoundIndex));

            CreateMap<PlayerInputDTO, Player>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id));

            CreateMap<Player, PlayerOutputDTO> ()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Coins, opt => opt.MapFrom(src => src.Coins))
                .ForMember(dest => dest.IsWaitingMatch, opt => opt.MapFrom(src => src.IsWaitingMatch));

            CreateMap<UpdatePlayerInputDTO, Player>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PlayerId))
                .ForMember(dest => dest.Coins, opt => opt.MapFrom(src => src.Coins))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.IsWaitingMatch, opt => opt.MapFrom(src => src.IsWaitingMatch));

            CreateMap<Turn, SendTurnOuputDTO>()
                .ForMember(dest => dest.TurnId, opt => opt.MapFrom(src => src.Id)); 

            CreateMap<GameSession, OpenGameSessionOutputDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id));

            CreateMap<GameSession, CreateGameSessionOutputDTO>()
                .ForMember(dest => dest.GameSessionID, opt => opt.MapFrom(src => src.Id));

            CreateMap<GameSession, JoinGameSessionOutputDTO>()
                .ForMember(dest => dest.GameSessionID, opt => opt.MapFrom(src => src.Id));
        }
    }
}
