﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PostmanAPI.DTOs;
using PostmanAPI.Model;
using PostmanAPI.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PostmanAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AnswerController : ControllerBase
    {
        public readonly IAnswerService _answerService;

        public AnswerController(IAnswerService answerService)
        {
            _answerService = answerService;
        }

        [HttpGet("GetAnswersByTurn")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AnswerOuput))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAnswersByTurn(string turnId)
        {
            try
            {
                var answers = await _answerService.GetAnswersByTurn(turnId);
                List<AnswerOuput> answerOuputs = new List<AnswerOuput>();

                foreach (var item in answers)
                {
                    answerOuputs.Add(new AnswerOuput
                    {
                        Topic = item.TopicName,
                        Input = item.Input,
                        IsCorrect = item.IsCorrect
                    });
                }

                return Ok(answerOuputs);
            }
            catch (Exception exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
            }
        }

        public async Task UpdateAnswer(Answer answer)
        {
            await _answerService.Update(answer);
        }
    }

}
