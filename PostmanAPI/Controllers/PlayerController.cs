﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PostmanAPI.DTOs;
using PostmanAPI.Model;
using System;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.Services;

namespace PostmanAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlayerController : ControllerBase
    {
        private readonly IPlayerService _playerService;
        private readonly IMapper _mapper;

        public PlayerController(IPlayerService playerService, IMapper mapper)
        {
            _playerService = playerService;
            _mapper = mapper;
        }

        [HttpGet("GetPlayer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PlayerOutputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetPlayer(string id)
        {
            try
            {
                var player = await _playerService.GetPlayer(id);
                if (player == null)
                {
                    return NotFound();
                }

                var output = _mapper.Map<PlayerOutputDTO>(player);

                return Ok(output);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("CreatePlayer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PlayerOutputDTO))]
        public async Task<IActionResult> CreatePlayer([FromBody] PlayerInputDTO inputDTO)
        {
            try
            {
                var player = _mapper.Map<Player>(inputDTO);
                player.Coins = 0;
                await _playerService.CreatePlayer(player);
                var outputDTO = _mapper.Map<PlayerOutputDTO>(player);
                return Ok(outputDTO);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("update")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdatePlayer([FromBody] UpdatePlayerInputDTO player)
        {
            try
            {
                var changedPlayer = _mapper.Map<Player>(player);
                await _playerService.UpdatePlayer(changedPlayer);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("UpdatePlayerName")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdatePlayerName([FromBody] UpdatePlayerNameInputDTO updatePlayerNameDTO)
        {
            try
            {
                var player = await _playerService.GetPlayer(updatePlayerNameDTO.PlayerId);

                if (player == null)
                {
                    return NotFound();
                }

                player.Name = updatePlayerNameDTO.Name;
                await _playerService.UpdatePlayer(player);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("ClaimGold")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ClaimGold([FromBody] string playerId)
        {
            try
            {
                var player = await _playerService.GetPlayer(playerId);

                if (player == null)
                {
                    return NotFound();
                }

                player.Coins += 100;
                await _playerService.UpdatePlayer(player);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
