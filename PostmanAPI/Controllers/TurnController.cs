﻿using Microsoft.AspNetCore.Mvc;
using PostmanAPI.DTOs;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Http;
using System.Linq;
using AutoMapper;
using PostmanAPI.Interfaces.Services;
using System.Collections.Generic;
using PostmanAPI.Services;
using PostmanAPI.Model;

namespace PostmanAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TurnController : ControllerBase
    {
        private readonly ITurnService _turnService;
        private readonly ITopicService _topicService;
        private readonly IMapper _mapper;
        private readonly IAnswerService _answerService;
        public TurnController(ITurnService turnService, ITopicService topicService, IMapper mapper, IAnswerService answerService)
        {
            _turnService = turnService;
            _topicService = topicService;
            _mapper = mapper;
            _answerService = answerService;
        }

        [HttpGet("GetTurn")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TurnOutput))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetTurn(string roundId, string playerId)
        {
            try
            {
                var turn = await _turnService.GetTurn(roundId, playerId);
                if (turn == null)
                {
                    return NotFound();
                }

                var turnoutput = new TurnOutput
                {
                    TurnId = turn.Id,
                };

                return Ok(_mapper.Map<TurnOutput>(turn));
            }
            catch (Exception exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
            }
        }

        [HttpPost("SendTurnInput")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SendTurnOuputDTO))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SendTurnInput([FromBody] TurnInputDTO turnInputDTO)
        {
            try
            {
                var turn = await _turnService.CreateTurn(turnInputDTO.RoundId, turnInputDTO.PlayerId);

                foreach (var answerInput in turnInputDTO.AnswerInputs)
                {
                    Answer answer = new Answer
                    {
                        TurnId = turn.Id,
                        Input = answerInput.Input,
                        TopicName = answerInput.TopicName
                    };

                    await _answerService.Create(answer);
                }

                return Ok(_mapper.Map<SendTurnOuputDTO>(turn));
            }
            catch (Exception exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
            }
        }

        [HttpPost("CheckTurn")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CheckTurnOutput))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CheckTurn([FromBody] CheckTurnInput checkTurnInput)
        {
            try
            {
                var turn = await _turnService.GetTurnById(checkTurnInput.TurnId);
                if (turn == null)
                {
                    return NotFound();
                }

                var wordsByTopic = await _topicService.GetWordsByTopic(checkTurnInput.Topics);
                var answers = await _answerService.GetAnswersByTurn(turn.Id);

                List<bool> results = new List<bool>();
                var dto = new CheckTurnOutput();
                List<string> inputs = new List<string>();

                foreach (var answer in answers)
                {
                    if (wordsByTopic.TryGetValue(answer.TopicName, out var words))
                    {
                        inputs.Add(answer.Input);
                        answer.Validate(words, checkTurnInput.Letter);
                        await _answerService.Update(answer);
                        results.Add(answer.IsCorrect);
                    }
                }

                //turn.Checks = _turnService.CheckTurnInputs(wordsByTopic, turn.Input.ToArray(), checkTurnInput.Letter);
                //_turnService.CalculateScore(results.ToArray());
                //turn.Checks = results;
                turn.CalculateScore(results.ToArray());
                dto.Checks = results;
                dto.Input = inputs;
                dto.TurnId = turn.Id;

                //var map = _mapper.Map<CheckTurnOutput>(turn);
                await _turnService.UpdateTurn(turn);

                return Ok(dto);
            }
            catch (Exception exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, exception.Message);
            }
        }
    }
}


