﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PostmanAPI.DTOs;
using PostmanAPI.Model;
using PostmanAPI.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using PostmanAPI.Interfaces.Services;
using PostmanAPI.Repository;
using System.Collections.Generic;

namespace PostmanAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RoundController : ControllerBase
    {
        private readonly IRoundService _roundService;
        private readonly ITopicService _topicService;
        private readonly IMapper _mapper;
        private readonly IGameSessionService _gameSessionService;

        public RoundController(IRoundService roundService, ITopicService topicService, IMapper mapper, IGameSessionService gameSessionService)
        {
            _roundService = roundService;
            _topicService = topicService;
            _mapper = mapper;
            _gameSessionService = gameSessionService;
        }

        [HttpPost("ObtainRoundForSession")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RoundOutputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ObtainRoundForSession([FromBody] RoundInputDTO roundInputDTO)
        {
            try
            {
                var gamesession = await _gameSessionService.GetGameSessionById(roundInputDTO.GamesessionId);
                if (gamesession == null)
                {
                    NotFound();
                }

                var queryResult = await _roundService.Query(
                    new QueryCondition("RoundNumber", roundInputDTO.RoundIndex, QueryOperator.EqualTo),
                    new QueryCondition("GameSessionId", roundInputDTO.GamesessionId, QueryOperator.EqualTo)
               );

                var round = queryResult.FirstOrDefault();
                var letters = new List<string>();

                if (round == null)
                {
                    var topics = await _topicService.GetRandomUniqueTopicsAsync(5);

                    if (gamesession.Letters != null)
                    {
                        letters = gamesession.Letters;
                    }

                    AlphabetGenerator alphabetGenerator = new AlphabetGenerator();
                    char letter = alphabetGenerator.GetRandomLetter(gamesession.Letters.ToArray());
                    round = await _roundService.CreateRound(roundInputDTO.GamesessionId, roundInputDTO.RoundIndex, letter, topics);
                    letters.Add(letter.ToString());
                    gamesession.Letters = letters;
                    await _gameSessionService.UpdateGameSession(gamesession);
                }

                var roundOutputDTO = _mapper.Map<RoundOutputDTO>(round);
                roundOutputDTO.IsLastRound = round.RoundNumber == GameConfig.MAX_ROUNDS;

                return Ok(roundOutputDTO);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("GetRoundForSession")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RoundOutputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetRoundForSession([FromBody] RoundInputDTO roundInputDTO)
        {
            try
            {
                var gamesession = await _gameSessionService.GetGameSessionById(roundInputDTO.GamesessionId);
                if (gamesession == null)
                {
                    NotFound();
                }

                var queryResult = await _roundService.Query(
                    new QueryCondition("RoundNumber", roundInputDTO.RoundIndex, QueryOperator.EqualTo),
                    new QueryCondition("GameSessionId", roundInputDTO.GamesessionId, QueryOperator.EqualTo)
               );

                var round = queryResult.FirstOrDefault();
                if (round == null)
                {
                    return NotFound();
                }

                var roundOutputDTO = _mapper.Map<RoundOutputDTO>(round);
                roundOutputDTO.IsLastRound = round.RoundNumber == GameConfig.MAX_ROUNDS;

                return Ok(roundOutputDTO);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("GetCurrentRound")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RoundOutputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetCurrentRound(string gamesessionId, string roundIndex, string isOwner)
        {
            try
            {
                int roundNumber = Convert.ToInt32(roundIndex);
                bool bIsOwner = Convert.ToBoolean(isOwner);
                var beforeRound = await _roundService.GetRound(gamesessionId, GetBeforeRoundIndex(roundNumber));
                bool hasPlayerCheck = (bIsOwner && beforeRound.OwnerResultCheck) || (!bIsOwner && beforeRound.ChallengerResultCheck);

                if (!hasPlayerCheck)
                {
                    var roundOutputDTOBefore = _mapper.Map<RoundOutputDTO>(beforeRound);
                    roundOutputDTOBefore.IsLastRound = ChecksIsLastRound(beforeRound);
                    return Ok(roundOutputDTOBefore);
                }

                var round = await _roundService.GetRound(gamesessionId, roundNumber);

                if (round == null)
                {
                    return NotFound();
                }

                var roundOutputDTO = _mapper.Map<RoundOutputDTO>(round);
                roundOutputDTO.IsLastRound = ChecksIsLastRound(round);
                return Ok(roundOutputDTO);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
            
        [HttpPost("CheckRound")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CheckRound([FromBody] RoundChecker roundChecker)
        {
            try
            {
                var round = await _roundService.GetRound(roundChecker.RoundId);

                if (round == null)
                {
                    return NotFound();
                }

                if (roundChecker.OwnerId == roundChecker.PlayerCheckerId)
                {
                    round.OwnerResultCheck = true;
                }
                else if (roundChecker.ChallengerId == roundChecker.PlayerCheckerId)
                {
                    round.ChallengerResultCheck = true;
                }

                await _roundService.UpdateRound(round);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        private bool ChecksIsLastRound(Round beforeRound)
        {
            return beforeRound.RoundNumber == GameConfig.MAX_ROUNDS;
        }

        private int GetBeforeRoundIndex(int roundNumber)
        {
            return Math.Abs(roundNumber - 1) == 0 ? 1 : roundNumber - 1;
        }
    }
}
