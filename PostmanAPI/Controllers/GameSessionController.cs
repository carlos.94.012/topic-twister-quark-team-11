﻿using Microsoft.AspNetCore.Mvc;
using PostmanAPI.DTOs;
using PostmanAPI.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using System.Linq;
using PostmanAPI.Interfaces.Services;
using PostmanAPI.Repository.GameSessionRepository;
using System.Threading;

namespace PostmanAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameSessionController : ControllerBase
    {
        private readonly IGameSessionService _gameSessionService;
        private readonly IPlayerService _playerService;
        private readonly IRoundService _roundService;
        private readonly ITurnService _turnService;
        private readonly IMapper _mapper;
        private readonly IGameSessionRepository _gameSessionRepository;

        public GameSessionController(IGameSessionService gameSessionsService,
                                     IPlayerService playerService,
                                     IRoundService roundService,
                                     ITurnService turnService,
                                     IMapper mapper, IGameSessionRepository gameSessionRepository)
        {
            _gameSessionService = gameSessionsService;
            _playerService = playerService;
            _roundService = roundService;
            _turnService = turnService;
            _mapper = mapper;
            _gameSessionRepository = gameSessionRepository;
        }

        [HttpGet("FindMatch")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CreateGameSessionOutputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> FindMatch(string playerId)
        {
            try
            {
                var findGameSessionUseCase = new FindGameSessionUseCase(_playerService, _gameSessionRepository, _mapper);
                var gameSession = await findGameSessionUseCase.Find(playerId);
                
                if (gameSession == null)
                {
                    return NotFound();
                }

                return Ok(gameSession);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("FirstOpen")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(OpenGameSessionOutputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> FirstOpen(string playerId)
        {
            try
            {
                IEnumerable<GameSession> gamesessions = await _gameSessionService.GetGameSessions();
                GameSession firstOpenGamesession = gamesessions.FirstOrDefault(
                    session => !session.IsClose && session.ChallengerId != playerId && session.OwnerId != playerId
                );

                if (firstOpenGamesession == null)
                {
                    return NotFound();
                }

                return Ok(_mapper.Map<OpenGameSessionOutputDTO>(firstOpenGamesession));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("Create")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CreateGameSessionOutputDTO))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateGameSession([FromBody] CreateGameSessionInputDTO createGameSessionInputDTO)
        {
            try
            {
                GameSession gameSession = await _gameSessionService.CreateGameSession(createGameSessionInputDTO.PlayerId);
                return Ok(_mapper.Map<CreateGameSessionOutputDTO>(gameSession));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("Join")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JoinGameSessionOutputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> JoinGameSession([FromBody] JoinGameSessionInputDTO joinGameSessionDTO)
        {
            try
            {
                if (string.IsNullOrEmpty(joinGameSessionDTO.GameSessionId))
                {
                    return BadRequest("Game session ID is required.");
                }
                else if (string.IsNullOrEmpty(joinGameSessionDTO.PlayerId))
                {
                    return BadRequest("Player ID is required.");
                }

                GameSession gameSession = await _gameSessionService.GetGameSessionById(joinGameSessionDTO.GameSessionId);

                if (gameSession == null)
                {
                    return NotFound("Game session not found.");
                }
                else if (gameSession.IsClose)
                {
                    return BadRequest("Game session is already closed.");
                }

                if (gameSession.OwnerId == joinGameSessionDTO.PlayerId ||
                    gameSession.ChallengerId == joinGameSessionDTO.PlayerId)
                {
                    return BadRequest("Player ID is already in this game session.");
                }

                if (!string.IsNullOrEmpty(gameSession.ChallengerId))
                {
                    return BadRequest("Game session is full.");
                }

                gameSession.ChallengerId = joinGameSessionDTO.PlayerId;

                if (!string.IsNullOrEmpty(gameSession.OwnerId) &&
                    !string.IsNullOrEmpty(gameSession.ChallengerId))
                {
                    gameSession.IsClose = true;
                }

                await _gameSessionService.UpdateGameSession(gameSession);
                return Ok(_mapper.Map<JoinGameSessionOutputDTO>(gameSession));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("GetFinished")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<GameSessionHistoricalDTO>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetFinishedSessions(string playerId)
        {
            try
            {
                var gameSessionHistoricalDTOs = new List<GameSessionHistoricalDTO>();
                IEnumerable<GameSession> gameSessions = await _gameSessionService.GetGameSessions();

                foreach (var gameSession in gameSessions)
                {
                    if (_gameSessionService.HasPlayer(gameSession, playerId)
                        && _gameSessionService.PlayerHasCheckedResult(gameSession, playerId)
                        && gameSession.IsFinished)
                    {
                        var opponentID = _gameSessionService.GetOpponentId(gameSession, playerId);

                        var opponent = await _playerService.GetPlayer(opponentID); //we can split this 
                        var playerWinner = await _playerService.GetPlayer(gameSession.PlayerWinnerID); //we can split this 

                        var gameSessionHistoricalDTO = new GameSessionHistoricalDTO
                        {
                            WinnerName = playerWinner?.Name,
                            OponentName = opponent?.Name
                        };

                        gameSessionHistoricalDTOs.Add(gameSessionHistoricalDTO);
                    }
                }
                return Ok(gameSessionHistoricalDTOs);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("GetActives")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<GameSessionActiveDTO>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetActiveSessions(string playerId)
        {
            try
            {
                var gameSessions = await _gameSessionService.GetGameSessions();
                var activeSessions = new List<GameSessionActiveDTO>();

                foreach (var gameSession in gameSessions)
                {
                    if (_gameSessionService.HasPlayer(gameSession, playerId) && (!_gameSessionService.PlayerHasCheckedResult(gameSession, playerId) || !gameSession.IsFinished))
                    {
                        var lastRoundIndex = Math.Abs(gameSession.RoundIndex - 1) == 0 ? 1 : gameSession.RoundIndex - 1;
                        var lastRound = await _roundService.GetRound(gameSession.Id, lastRoundIndex);
                        var currentRound = await _roundService.GetRound(gameSession.Id, gameSession.RoundIndex);
                        bool isOwner = _gameSessionService.CheckIsOwner(gameSession, playerId);

                        if (lastRound != null && currentRound != null)
                        {
                            bool hasResultCheck = (isOwner && lastRound.OwnerResultCheck) || (!isOwner && lastRound.ChallengerResultCheck);
                            int roundNumber = (lastRound != null && !hasResultCheck) ? lastRoundIndex : currentRound.RoundNumber;
                            var opponentId = _gameSessionService.GetOpponentId(gameSession, playerId);
                            var playerScore = _gameSessionService.GetPlayerScore(gameSession, playerId);
                            var opponentScore = _gameSessionService.GetPlayerScore(gameSession, opponentId);
                            var playerTurn = await _turnService.GetTurn(currentRound.Id, playerId);
                            var opponentTurn = await _turnService.GetTurn(currentRound.Id, opponentId);

                            Player opponent = null;

                            if (!string.IsNullOrEmpty(opponentId))
                            {
                                opponent = await _playerService.GetPlayer(opponentId);
                            }

                            activeSessions.Add(new GameSessionActiveDTO
                            {
                                OpponentName = opponent?.Name,
                                RoundNumber = roundNumber,
                                PlayerScore = playerScore,
                                OpponentScore = opponentScore,
                                GameSessionId = gameSession.Id,
                                WaitForOpponentTurn = opponentTurn == null,
                                WaitForPlayerTurn = playerTurn == null,
                                HasResultCheck = hasResultCheck,
                            });
                        }
                    }
                }
                return Ok(activeSessions);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("GetFirstOpen")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetFirstOpen([FromQuery] string playerId)
        {
            try
            {
                IEnumerable<GameSession> gamesessions = await _gameSessionService.GetGameSessions();
                GameSession gamesession = gamesessions.FirstOrDefault(session => !session.IsClose && session.OwnerId != playerId && session.ChallengerId != playerId);

                if (gamesession == null)
                {
                    return NotFound();
                }

                return Ok(_mapper.Map<OpenGameSessionOutputDTO>(gamesession));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("ById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GameSessionOutputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetGameSessionById([FromQuery] string id)
        {
            try
            {
                var gameSession = await _gameSessionService.GetGameSessionById(id);
                if (gameSession == null)
                {
                    return NotFound();
                }

                return Ok(_mapper.Map<GameSessionOutputDTO>(gameSession));
            }
            catch (Exception exeception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, exeception.Message);
            }
        }

        [HttpPost("SetNextRound")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SetNextRound([FromBody] NextRoundInputDTO nextRoundInputDTO)
        {
            try
            {
                GameSession gameSession = await _gameSessionService.GetGameSessionById(nextRoundInputDTO.GamesessionId);
                if (gameSession == null)
                {
                    return NotFound("Game session not found.");
                }
                else if (gameSession.IsFinished)
                {
                    return BadRequest("The current Game session is finished");
                }

                if (nextRoundInputDTO.RoundIndex == gameSession.RoundIndex
                    && gameSession.RoundIndex < GameConfig.MAX_ROUNDS)
                {
                    gameSession.RoundIndex++;
                    await _gameSessionService.UpdateGameSession(gameSession);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

            return Ok();
        }

        [HttpPost("HandleGameSessionWinner")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GameSessionResultOuputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> HandleGameSessionWinner([FromBody] HandleGameSessionWinnerInput handleGameSessionWinnerInput)
        {
            try
            {
                GameSession gameSession = await _gameSessionService.GetGameSessionById(handleGameSessionWinnerInput.GameSessionId);
                if (gameSession == null)
                {
                    return NotFound("Game session not found.");
                }
                gameSession.IsFinished = true;

                if (gameSession.OwnerScore == gameSession.ChallengerScore)
                {
                    await _gameSessionService.UpdateGameSession(gameSession);
                    return Ok(_mapper.Map<GameSessionResultOuputDTO>(gameSession));
                }

                gameSession.PlayerWinnerID = gameSession.OwnerScore > gameSession.ChallengerScore ? gameSession.OwnerId : gameSession.ChallengerId;
                await _gameSessionService.UpdateGameSession(gameSession);
                return Ok(_mapper.Map<GameSessionResultOuputDTO>(gameSession));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("CheckGameSession")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CheckGameSession([FromBody] GameSessionChecker gameSessionChecker)
        {
            try
            {
                GameSession gameSession = await _gameSessionService.GetGameSessionById(gameSessionChecker.GameSessionId);

                if (gameSession == null)
                {
                    return NotFound();
                }

                if (gameSession.OwnerId == gameSessionChecker.PlayerCheckerId)
                {
                    gameSession.OwnerResultCheck = true;
                }
                else if (gameSession.ChallengerId == gameSessionChecker.PlayerCheckerId)
                {
                    gameSession.ChallengerResultCheck = true;
                }

                await _gameSessionService.UpdateGameSession(gameSession);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost("HandleRoundResult")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RoundResultOutputDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> HandleRoundResult([FromBody] HandleRoundResultInput handleRoundResultInput)
        {
            try
            {
                var round = await _roundService.GetRound(handleRoundResultInput.RoundId);
                if (round == null)
                {
                    return NotFound("Round not found.");
                }

                var gameSession = await _gameSessionService.GetGameSessionById(round.GameSessionId);
                if (gameSession == null)
                {
                    return NotFound("Game session not found.");
                }

                var roundResult = _mapper.Map<RoundResultOutputDTO>(round);
                roundResult.ChallengerScore = gameSession.ChallengerScore;
                roundResult.OwnerScore = gameSession.OwnerScore;

                var ownerTurn = await _turnService.GetTurn(handleRoundResultInput.RoundId, gameSession.OwnerId);
                if (ownerTurn != null)
                {
                    _mapper.Map(ownerTurn, roundResult);
                }

                var challengerTurn = await _turnService.GetTurn(handleRoundResultInput.RoundId, gameSession.ChallengerId);
                if (challengerTurn != null)
                {
                    _mapper.Map(challengerTurn, roundResult);
                }

                if (!round.Finished && ownerTurn != null && challengerTurn != null)
                {
                    var ownerScore = ownerTurn.Score;
                    var challengerScore = challengerTurn.Score;
                    var winnerId = ownerScore > challengerScore ? gameSession.OwnerId : (ownerScore < challengerScore ? gameSession.ChallengerId : null);

                    if (winnerId != null)
                    {
                        round.PlayerWinnerID = winnerId;
                        if (winnerId == gameSession.OwnerId)
                        {
                            gameSession.OwnerScore++;
                        }
                        else
                        {
                            gameSession.ChallengerScore++;
                        }

                        await _gameSessionService.UpdateGameSession(gameSession);
                    }

                    roundResult.OwnerScore = gameSession.OwnerScore;
                    roundResult.ChallengerScore = gameSession.ChallengerScore;
                    round.Finished = true;

                    await _roundService.UpdateRound(round);
                }

                roundResult.RoundWinnerId = round.PlayerWinnerID;
                return Ok(roundResult);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
