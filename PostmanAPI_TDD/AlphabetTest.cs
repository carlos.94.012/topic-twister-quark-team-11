using System;
using System.Diagnostics;
using NUnit.Framework;
using PostmanAPI.Services;

namespace PostmanAPI_TDD
{
    public class AlphabetTest
    {
        private AlphabetService _alphabetService;

        [SetUp]
        public void Setup()
        {
            _alphabetService = new AlphabetService();
        }

        [Test]
        public void CheckGetWordsByTopic()
        {
            string letters = "abcdefgilmnoprstu"; //All letters except j
            var result = _alphabetService.GetRandomLetter(letters);
            Assert.AreEqual('j', result);
        }
        
    }
}