﻿using NUnit.Framework;
using PostmanAPI.Services;
using System.Collections.Generic;
using NSubstitute;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Repository.TurnRepository;


namespace PostmanAPI_TDD
{
    class TurnTest
    {
        private TurnService _turnService;
        private TurnRepository _turnRepository;
        private ITurnDAO _turnDao = null;
        private List<string> _words;
        private Dictionary<string, List<string>> _topic;

        [SetUp]
        public void Setup()
        {
            _turnRepository = Substitute.For<TurnRepository>(_turnDao);
            _turnService = new TurnService(_turnRepository);
        }

        [TestCase("perro", "p", ExpectedResult = true)]
        [TestCase("ballena", "b", ExpectedResult = true)]
        [TestCase("gato", "c", ExpectedResult = false)]
        [TestCase("caballo", "c", ExpectedResult = false)]
        public bool CheckIfWordInputIsRight(string input, string letter)
        {
            _words = new List<string>() { "ballena", "perro", "gato", "vaca", "gacela", "grillo", };
            return _turnService.IsInputCorrect(input, _words, letter);
        }


        [TestCase(new bool[] { true, true, true }, ExpectedResult = 3)]
        [TestCase(new bool[] { true, false, true }, ExpectedResult = 2)]
        [TestCase(new bool[] { false, true, false }, ExpectedResult = 1)]
        [TestCase(new bool[] { false, false, true }, ExpectedResult = 1)]
        [TestCase(new bool[] { false, false, false }, ExpectedResult = 0)]
        public int CountCorrectInputs(bool[] checks)
        {
            return _turnService.CalculateScore(checks);
        }

        [TestCase(new string[] { "gato", "grillo", "gacela" }, "g", ExpectedResult = new bool[] { true, true, true })]
        [TestCase(new string[] { "gato", "grillo", "gacela" }, "c", ExpectedResult = new bool[] { false, false, false })]
        [TestCase(new string[] { "gato", "grillo", "caballo" }, "c", ExpectedResult = new bool[] { false, false, true })]
        [TestCase(new string[] { "croacia", "", "cancun" }, "c", ExpectedResult = new bool[] { false, false, false })]
        public bool[] CheckTurnInputsAsBoolArray(string[] inputs, string letter)
        {
            _words = new List<string>() { "ballena", "perro", "gato", "vaca", "gacela", "grillo", "caballo"};
            _topic = new Dictionary<string, List<string>>()
            {
                { "animales", _words },
                { "animales1", _words },
                { "animales2", _words }
            };
            return _turnService.CheckTurnInputs(_topic, inputs, letter); ;
        }
    }
}

