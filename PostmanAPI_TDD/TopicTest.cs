using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using PostmanAPI.Interfaces.DataAccessObject;
using PostmanAPI.Model;
using PostmanAPI.Repository.TopicRepository;
using PostmanAPI.Services;

namespace PostmanAPI_TDD
{
    public class TopicTest
    {
        private TopicService _topicService;
        private TopicRepository _topicRepository;
        private ITopicDAO _topicDao = null;


        [SetUp]
        public void Setup()
        {
            _topicDao = Substitute.For<ITopicDAO>(null);
            _topicRepository = Substitute.For<TopicRepository>(_topicDao);
            _topicService = new TopicService(_topicRepository);
        }

        [Test]
        public void CheckGetWordsByTopic()
        {
            var topicMockAnimals = new Topic()
            {
                Name = "animals",
                words = new List<string>() { "ballena", "perro", "gato", "vaca", "gacela", "grillo", "caballo" }
            };
            
            var topicMockCountries = new Topic()
            {
                Name = "countries",
                words = new List<string>() { "rusia", "argentina", "iran", "irak", "nepal", "china", "japon" }
            };
            
            _topicRepository.GetAsync("animals").Returns(topicMockAnimals);
            _topicRepository.GetAsync("countries").Returns(topicMockCountries);
            
             var result = _topicService.GetWordsByTopic(new[] { "countries", "animals" });
            
             Assert.AreEqual(result.Result, new Dictionary<string, List<string>>()
             {
                 {"animals", new List<string>() {"ballena", "perro", "gato", "vaca", "gacela", "grillo", "caballo"}},
                 {"countries", new List<string>() {"rusia", "argentina", "iran", "irak", "nepal", "china", "japon"}}
             });
        }
    }
}