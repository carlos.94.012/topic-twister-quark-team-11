﻿using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using UnityEngine;
using System;

namespace TopicTwister.GateWay
{
    public class HTTPGateway
    {
        private const string UrlBase = "https://localhost:44377/"; //"https://topictwisterapi.azurewebsites.net/"; /*;*/
        //"";

        private readonly HttpClient _client = new();

        public async Task<HttpResponseMessage> Get(string uri)
        {
            return await _client.GetAsync(UrlBase + uri);
        }

        public async Task<HttpResponseMessage> Post<T>(string uri, T data)
        {
            var content = CreateStringContent(data);
            return await _client.PostAsync(UrlBase +  uri, content);
        }

        public async Task<HttpResponseMessage> Put<T>(string uri, T data)
        {
            var content = CreateStringContent(data);
            return await _client.PutAsync(UrlBase + uri, content);
        }

        public async Task<HttpResponseMessage> Delete(string uri)
        {
            return await _client.DeleteAsync(uri);
        }

        private StringContent CreateStringContent<T>(T data)
        {
            var content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");
            return content;
        }
    }
}