﻿using UnityEngine;
using TopicTwister.Presentation;
using TMPro;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using TopicTwister.Tools;
using System.Collections.Generic;
using System.Collections;

namespace TopicTwister.Delivery
{
    public class ActiveGameSessionView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _data;
        [SerializeField] private TextMeshProUGUI _opponentName;
        [SerializeField] private TextMeshProUGUI _status;
        [SerializeField] private Button _openGameSessionButton;

        private string _gameSessionId;
        private ActiveGameSessionPresenter _activeGameSessionPresenter;
        public Action OnOpenGameSession;
        private bool _waitForPlayerTurn;
        private bool _hasResultCheck;
        private Animator _animator;

        private void Start()
        {
            _activeGameSessionPresenter = new(this);
            _openGameSessionButton.onClick.AddListener(OnLoadGameSession);
            _animator = GameObject.Find("MainMenu").GetComponent<Animator>();
        }

        public string GetGameSessionId()
        {
            return _gameSessionId;
        }

        public void Setup(string opponentName, int roundNumber, string gameSessionId, bool waitForPlayerTurn, bool waitForOpponentTurn, bool HasResultCheck, int playerScore, int opponentScore)
        {
            _data.text = $"Ronda {roundNumber} {playerScore} - {opponentScore}";
            _opponentName.text = opponentName;
            _gameSessionId = gameSessionId;
            _status.text = waitForPlayerTurn || !waitForOpponentTurn ? "<color=#FF9400>Es tu turno</color>" : "No es tu turno!";
        
            _waitForPlayerTurn = waitForPlayerTurn;
            _hasResultCheck = HasResultCheck;
        }

        private void OnLoadGameSession()
        {
            StartCoroutine(LoadGameSessionCoroutine());
        }

        private IEnumerator LoadGameSessionCoroutine()
        {
            _animator.Play("anim_MainMenu_Hide");
            yield return new WaitForSeconds(1.5f);
            OnOpenGameSession.Invoke();

            if (_waitForPlayerTurn)
            {
                SceneManager.LoadScene("Gameplay");
            }
            else if (_hasResultCheck || !_waitForPlayerTurn)
            {
                SceneManager.LoadScene("Result");
            }
        }
    }
}