using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace TopicTwister.Delivery
{
    public class ResultBanner : MonoBehaviour
    {
        [SerializeField] Sprite crossSprite, checkSprite;
        [SerializeField] TextMeshProUGUI topicTxt;
        [SerializeField] ResultInfo _playerResultInfo, _oponentResultInfo;

        public void SetInput(ResultInfo resultInfo, string input)
        {
            resultInfo.answerText.text = input;
        }

        public void SetTopic(string topic)
        {
            topicTxt.text = topic;
        }

        public string GetTopic()
        {
            return topicTxt.text;
        }

        public void SetCheck(ResultInfo resultInfo, bool isRight)
        {
            resultInfo.resultImage.sprite = !isRight ? crossSprite : checkSprite;
        }

        public ResultInfo GetResultInfo(bool currentPlayer)
        {
            return currentPlayer ? _playerResultInfo : _oponentResultInfo;
        }
    }
}

[System.Serializable]
public class ResultInfo
{
    [SerializeField] public TextMeshProUGUI answerText;
    [SerializeField] public Image resultImage;
}