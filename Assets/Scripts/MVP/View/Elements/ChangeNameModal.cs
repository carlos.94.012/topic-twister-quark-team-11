using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace TopicTwister.Delivery
{
    public class ChangeNameModal : MonoBehaviour
    {
        [SerializeField] Button acceptButton;
        [SerializeField] TMP_InputField inputName;

        public Action<string> OnChangeName;
        
        private void OnEnable()
        {
            inputName.onValueChanged.AddListener(AcceptButtonSet);
            acceptButton.onClick.AddListener(ChangeName);
        }

        private void OnDisable()
        {
            inputName.onValueChanged.RemoveAllListeners();
        }

        private void AcceptButtonSet(string value)
        {
            acceptButton.interactable = !string.IsNullOrEmpty(value);
        }

        private void ChangeName()
        {
            OnChangeName?.Invoke(inputName.text);
            gameObject.SetActive(false);
        }
    }
}