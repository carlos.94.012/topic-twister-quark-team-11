using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace TopicTwister.Delivery
{
    public class HistoricalGameElement : MonoBehaviour
    {
        [SerializeField] private Sprite winSprite, loseSprite, tieSprite;
        [SerializeField] private Sprite backgroundWinSprite, backgroundLoseSprite, backgroundTieSprite;
        [SerializeField] private TextMeshProUGUI oponentGame;
        [SerializeField] private Image backgroundImage;
        [SerializeField] private Image resultImage;

        public void SetHistoricalGame(string winnerName, string playerOponentName)
        {
            oponentGame.text = playerOponentName;
            if (String.IsNullOrWhiteSpace(winnerName))
            {
                backgroundImage.sprite = backgroundTieSprite;
                resultImage.sprite = tieSprite;
                return;
            }

            var iWon = winnerName != playerOponentName;
            backgroundImage.sprite = iWon ? backgroundWinSprite : backgroundLoseSprite;
            resultImage.sprite = iWon ? winSprite : loseSprite;
        }
    }
}