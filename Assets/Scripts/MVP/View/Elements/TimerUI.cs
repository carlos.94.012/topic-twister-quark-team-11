using System;
using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

namespace TopicTwister.Delivery
{
    public class TimerUI : MonoBehaviour
    {
        [SerializeField] private float _totalTime;
        [SerializeField] private TextMeshProUGUI _timerDisplay;
        public event Action OnFinish;

        public IEnumerator DecrementTimer()
        {
            _timerDisplay.text = _totalTime.ToString();

            while (_totalTime > 0)
            {
                yield return new WaitForSeconds(1);
                _totalTime--;
                _timerDisplay.text = _totalTime.ToString();
            }
            OnFinish?.Invoke();
        }

        public void SetTotalTime(float TotalTime)
        {
            _totalTime = TotalTime;
        }
    }
}
