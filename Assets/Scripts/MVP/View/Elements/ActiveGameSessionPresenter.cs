﻿using TopicTwister.Delivery;
using TopicTwister.DTOs;
using TopicTwister.Tools;

namespace TopicTwister.Presentation
{
    public class ActiveGameSessionPresenter
    {
        private readonly ActiveGameSessionView _view;

        public ActiveGameSessionPresenter(ActiveGameSessionView view)
        {
            _view = view;
            _view.OnOpenGameSession += OnOpenGameSession;
        }

        private void OnOpenGameSession()
        {
            var player = SaveSystem.LoadData<PlayerOutputDTO>("/Player");
            player.CurrentGameSessionId = _view.GetGameSessionId();
            SaveSystem.SaveData(player, "/Player");
        }
    }
}