﻿using UnityEngine;
using TopicTwister.Presentation;
using System;

namespace TopicTwister.Delivery
{
    public class ActiveGameSessionsListView : MonoBehaviour
    {
        [SerializeField] ActiveGameSessionView _activeGameBannerPrefab;
        [SerializeField] Transform _content;

        private ActiveGamesListPresenter _activeGameListPresenter;
        public Action OnInit;

        public void Start()
        {
            _activeGameListPresenter = new(this);
            OnInit.Invoke();
        }

        public void ShowActiveGame(string gameSessionId, string opponentName, int roundNumber, bool waitForPlayerTurn, bool waitForOpponentTurn, bool hasResultCheck, int playerScore, int opponentScore)
        {
            ActiveGameSessionView newHist = Instantiate(_activeGameBannerPrefab, _content);
            newHist.Setup(opponentName, roundNumber, gameSessionId, waitForPlayerTurn, waitForOpponentTurn, hasResultCheck, playerScore, opponentScore);
        }
    }
}