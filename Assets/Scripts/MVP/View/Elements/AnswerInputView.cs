﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace TopicTwister.Delivery
{
    public class AnswerInputView : Selectable
    {
        [SerializeField] TextMeshProUGUI categoryText;
        [SerializeField] TMP_InputField _input;
        [SerializeField] Image _background;
        [SerializeField] Color _disabledColor;
        [SerializeField] Color _enabledColor;

        [SerializeField] public Image[] _categoryDeco;
        [SerializeField] public Sprite _categoryDecoSelected;
        [SerializeField] public Sprite _categoryDecoUnselected;

        public TextMeshProUGUI CategoryText { get => categoryText; set => categoryText = value; }
        public TMP_InputField Input { get => _input; set => _input = value; }

  

        public void TurnOff()
        {
            _background.color = _disabledColor;
            _input.interactable = false;
        }

        public void TurnOn()
        {
            _background.color = _enabledColor;
            _input.interactable = true;
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            foreach (var item in _categoryDeco)
            {
                item.sprite = _categoryDecoSelected;
            }
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            foreach (var item in _categoryDeco)
            {
                item.sprite = _categoryDecoUnselected;
            }
        }

    }
}