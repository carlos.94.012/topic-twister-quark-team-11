using System;
using UnityEngine;
using UnityEngine.UI;
using TopicTwister.Presentation;

namespace TopicTwister.Delivery
{
    public class HistoricalGameModalView : MonoBehaviour
    {
        [SerializeField] Button _closeButton;
        [SerializeField] HistoricalGameElement _historicalGameBannerPrefab;
        [SerializeField] Transform _content;

        private HistoricalGameModalPresenter _historicalGameModalPresenter;
        public Action OnOpenHistoricalGameModal;

        void Start()
        {
            _historicalGameModalPresenter = new HistoricalGameModalPresenter(this);
            OnOpenHistoricalGameModal.Invoke();
        }

        private void OnEnable()
        {
            _closeButton.onClick.AddListener(CloseModal);
        }

        private void OnDisable()
        {
            _closeButton.onClick.RemoveAllListeners();
        }

        public void ShowHistorical(string winnerName, string oponentName)
        {
            HistoricalGameElement newHist = Instantiate(_historicalGameBannerPrefab, _content);
            newHist.SetHistoricalGame(winnerName, oponentName);
        }

        private void CloseModal()
        {
            gameObject.SetActive(false);
        }
    }
}