using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace TopicTwister.Delivery
{
    public class PlayerInfoMainMenu : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _nameText;
        [SerializeField] private TextMeshProUGUI _coinsText;
        [SerializeField] private Button _playerInfoButton;

        public Button PlayerInfoButton => _playerInfoButton;

        public void SetPlayerInfo(string name, int coins)
        {
            _nameText.text = name;
            _coinsText.text = coins.ToString();
        }
    }
}