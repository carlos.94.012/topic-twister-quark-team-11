﻿using TopicTwister.Presentation;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

namespace TopicTwister.Delivery
{
    public class PlayerAnswersScreenView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _letter;
        [SerializeField] private TextMeshProUGUI _roundText;
        [SerializeField] private TextMeshProUGUI _money;

        [SerializeField] private Button _stopButton;
        [SerializeField] private AnswerInputView[] answers = new AnswerInputView[5];
        [SerializeField] private TimerUI _timerUI;
        [SerializeField] private Button _returnToMainMenuButton;

        public GameObject WaitPanel;
        public AnswerInputView[] Answers { get => answers; }

        public Action OnInit;
        public Action OnStopButtonAction;
        public Action OnCountDownFinishAction;
        public Action OnReturnToMainMenu;

        public Animator _animator;

        private void Start()
        {
            new PlayerAnswersPresenter(this);
            SetButtons();
            OnInit.Invoke();
        }

        private void SetButtons()
        {
            _stopButton.onClick.AddListener(FinishTurn);
            _returnToMainMenuButton.onClick.AddListener(ReturnToMainMenu);
            _timerUI.OnFinish += StopTimer;
        }

        public void SetRoundNumber(int index) => _roundText.text = $"Ronda {index}";
        public void SetRoundLetter(string roundLetter) => _letter.text = roundLetter;
        public void SetCategories(string[] categories)
        {
            for (var i = 0; i < answers.Length; i++)
            {
                answers[i].CategoryText.text = categories[i];
            }
        }

        public void FinishTurn()
        {
            StopAllCoroutines();
            StartCoroutine(StopButtonCorotuine());
        }

        public void SetMoney(int coins)
        {
            _money.text = coins.ToString();
        }

        public void StopTimer()
        {
            OnCountDownFinishAction.Invoke();
        }

        public void ReturnToMainMenu() => OnReturnToMainMenu.Invoke();

        public void ResetAnswersInput() => Array.ForEach(answers, item => item.Input.text = string.Empty);
        public string[] GetAnswerInput() => Answers.Select(answer => answer.Input.text).ToArray();

        public void TurnOffInput() => Array.ForEach(answers, item => item.TurnOff());
        public void TurnOnInput() => Array.ForEach(answers, item => item.TurnOn());
        
        public void InitTimer(float time)
        {
            StopAllCoroutines();
            _timerUI.SetTotalTime(time);
            StartCoroutine(_timerUI.DecrementTimer());
        }

        public void ShowStopScreen()
        {
            if (WaitPanel != null)
            {
                StopAllCoroutines();
                WaitPanel.SetActive(true);
                StartCoroutine(ShowStopCoroutine());
            }
        }

        private IEnumerator ShowStopCoroutine()
        {
            yield return new WaitForSeconds(1.5f);
            OnCountDownFinishAction.Invoke();
        }


        private IEnumerator StopButtonCorotuine()
        {
            _animator.Play("Anim_PlayerAnswer_Hide");
            yield return new WaitForSeconds(1.5f);
            OnStopButtonAction.Invoke();
        }
    }
}