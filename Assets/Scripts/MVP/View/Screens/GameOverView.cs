using System;
using TopicTwister.Presentation;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace TopicTwister.Delivery
{
    public class GameOverView : MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private TextMeshProUGUI _winnerDisplay;
        public Action OnButtonClick;

        private void Start()
        {
            _button.onClick.AddListener(OnButtonClick.Invoke);
        }

        internal void SetWinnerText(string value)
        {
            _winnerDisplay.text = value;
        }

        private void ActivateExitButton() => _button.gameObject.SetActive(true);
    }
}
