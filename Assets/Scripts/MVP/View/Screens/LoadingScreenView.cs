using System.Collections;
using TopicTwister.Presentation;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

namespace TopicTwister.Delivery
{
    public class LoadingScreenView : MonoBehaviour
    {
        private const float LOADING_BAR_MIN_DURATION = 5f;
        [SerializeField] private Slider loadingBar;
        [SerializeField] private Image background;
        [SerializeField] private float sleepTimeEnd;
        public Action OnInit;

        private void Start()
        {
            new LoadingScreenPresenter(this);
            OnInit.Invoke();
        }

        public void LoadLevel(string id)
        {
            StartCoroutine(LoadScene(id));
        }

        private IEnumerator LoadScene(string id)
        {
            float elapsed = 0;
            Color currentColor = Color.white;
            currentColor.a = 0;

            AsyncOperation asyncScene = SceneManager.LoadSceneAsync(id);
            asyncScene.allowSceneActivation = false;

            while (elapsed < LOADING_BAR_MIN_DURATION)
            {
                loadingBar.value = Mathf.Clamp01(elapsed / LOADING_BAR_MIN_DURATION);
                currentColor.a = loadingBar.value;
                background.color = currentColor;
                elapsed += Time.deltaTime;
                yield return null;
            }

            yield return new WaitForSecondsRealtime(sleepTimeEnd);
            asyncScene.allowSceneActivation = true;
        }
    }
}