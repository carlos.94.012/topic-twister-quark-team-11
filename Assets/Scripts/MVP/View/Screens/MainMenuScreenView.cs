using TopicTwister.Presentation;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace TopicTwister.Delivery
{
    public class MainMenuScreenView : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _historialButton;
        [SerializeField] private HistoricalGameModalView _historicalGameModal;
        [SerializeField] private ChangeNameModal _changeNameModal;
        [SerializeField] private PlayerInfoMainMenu _playerInfoBanner;
        [SerializeField] private ActiveGameSessionsListView _activeGames;

        private MainMenuScreenPresenter _mainMenuScreenPresenter;

        public Action OnPlayButtonClick;

        private void Start()
        {
            _mainMenuScreenPresenter = new MainMenuScreenPresenter(this);

            _playButton.onClick.AddListener(OnPlayButtonClick.Invoke);
            _playerInfoBanner.PlayerInfoButton.onClick.AddListener(OpenChangeNameModal);
            _historialButton.onClick.AddListener(OpenHistoricalGameModal);
        }

        public void ShowMenu()
        {
            _animator.Play("anim_MainMenu_Show");
        }

        public void HideMenu()
        {
            _animator.Play("anim_MainMenu_Hide");
            var animationData = _animator.GetCurrentAnimatorStateInfo(0);
            var quartSize = (animationData.length * 0.25f);
            LeanTween.delayedCall(gameObject, animationData.length - quartSize, () => SceneManager.LoadScene("Matchmaking"));
        }

        public void OpenChangeNameModal()
        {
            _changeNameModal.gameObject.SetActive(true);
        }

        public void OpenHistoricalGameModal()
        {
            _historicalGameModal.gameObject.SetActive(true);
        }

        public void ShowActiveGames()
        {
            _activeGames.gameObject.SetActive(true);
        }

        public void ShowPlayerInfo(string playerName, int coins)
        {
            _playerInfoBanner.SetPlayerInfo(playerName, coins);
        }

        public void AddActionWhenNameIsChanged(Action<string> action)
        {
            _changeNameModal.OnChangeName += action;
        }
    }
}