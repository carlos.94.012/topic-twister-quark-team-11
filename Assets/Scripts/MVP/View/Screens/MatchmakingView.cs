using System;
using TMPro;
using TopicTwister.Presentation;
using UnityEngine;

namespace TopicTwister.Delivery
{
    public class MatchmakingView : MonoBehaviour
    {
        public Action OnInit;
        private MatchmakingPresenter _matchmakingPresenter;
        [SerializeField] private TextMeshProUGUI _playerName;
        [SerializeField] private TextMeshProUGUI _opponentName;

        public MatchmakingView()
        {
            _matchmakingPresenter = new MatchmakingPresenter(this);
        }

        private void Start()
        {
            OnInit.Invoke();
        }

        public void SetPlayerName(string playerName)
        {
            _playerName.SetText(playerName);
        }

        public void SetOpponentName(string opponentName)
        {
            _opponentName.SetText(opponentName);
        }
    }
}
