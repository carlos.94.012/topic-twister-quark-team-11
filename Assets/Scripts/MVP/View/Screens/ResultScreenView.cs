using TopicTwister.Presentation;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using System.Linq;

namespace TopicTwister.Delivery
{
    public class ResultScreenView : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        
        [SerializeField] private Button _nextRound;
        [SerializeField] private Button _endGame;
        [SerializeField] private Button _passTheTurn;
        [SerializeField] private Button _claim;

        [SerializeField] private ResultBanner[] _resultsBanner;
        [SerializeField] private TextMeshProUGUI _roundIndex;
        [SerializeField] private TextMeshProUGUI _titleHeader;

        [SerializeField] private GameObject[] _playerScoreBoard;
        [SerializeField] private GameObject[] _opponenScoreBoard;

        private ResultScreenPresenter _resultScreenPresenter; 

        public Action OnNextRoundClick;
        public Action OnEndGameClick;
        public Action OnPassTheTurnClick;
        public Action OnClaim;

        public Action OnInit;
        public Image Courtain;

        public Animator Animator { get => _animator; }

        public Button NextRound { get => _nextRound; }
        public Button PassTheTurn { get => _passTheTurn; }
        public Button Claim { get => _claim;  }
        public Button EndGame { get => _endGame; }

        public void Start()
        {
            _resultScreenPresenter = new ResultScreenPresenter(this);
            _nextRound.onClick.AddListener(OnNextRoundClick.Invoke);
            EndGame.onClick.AddListener(OnEndGameClick.Invoke);
            _passTheTurn.onClick.AddListener(OnPassTheTurnClick.Invoke);
            _claim.onClick.AddListener(OnClaim.Invoke);
            OnInit.Invoke();
        }

        public void ShowMenu()
        {
            Animator.Play("anim_ResultScreen_Show");
        }
        public ResultBanner GetResultBanner(string topic)
        {
            return _resultsBanner.FirstOrDefault(resultBanner => resultBanner.GetTopic() == topic);
        }

        public void SetAnswer(bool currentPlayer, string topic, string input, bool check)
        {
            var resultBanner = GetResultBanner(topic);
            var resultInfo = resultBanner.GetResultInfo(currentPlayer);
            resultBanner.SetInput(resultInfo, input);
            resultBanner.SetCheck(resultInfo, check);
        }

        public void SetTopics(string[] topics)
        {
            for (int i = 0; i < topics.Length; i++)
            {
                _resultsBanner[i].SetTopic(topics[i]);
            }
        }

        public void SetRoundIndex(int i)
        {
            _roundIndex.text = $"Round {i}";
        }

        public GameObject[] GetScoreBoard(bool currentPlayer)
        {
            return currentPlayer ? _playerScoreBoard : _opponenScoreBoard;
        }

        public void SetScoreBoard(int score, GameObject[] scoreBoard)
        {
            for (int i = 0; i < scoreBoard.Length; i++)
            {
                if (score <= scoreBoard.Length && i < score)
                {
                    scoreBoard[i].SetActive(true);
                }
            }
        }

        public void ShowEndGameButton()
        {
            EndGame.gameObject.SetActive(true);
        }

        public void ShowPassTheTurnButton()
        {
            _passTheTurn.gameObject.SetActive(true);
        }

        public void ShowNextRoundButton()
        {
            _nextRound.gameObject.SetActive(true);
        }

        internal void ShowClaimButton()
        {
            _claim.gameObject.SetActive(true);
        }

        internal void SetHeaderText(string text)
        {
            _titleHeader.text = text;
        }
    }
}
