﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class CreateGameSessionInputDTO
    {
        public string PlayerId { get; set; }
    }
}