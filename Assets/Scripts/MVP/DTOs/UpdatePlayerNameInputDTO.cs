namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class UpdatePlayerNameInputDTO
    {
        public string PlayerId { get; set; }
        public string Name { get; set; }
    }
}