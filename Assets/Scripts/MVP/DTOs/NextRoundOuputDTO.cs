﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class NextRoundOuputDTO
    {
        public string RoundId { get; set; }
        public string OwnerTurnId { get; set; }
        public string ChallengerTurnId { get; set; }
    }
}
