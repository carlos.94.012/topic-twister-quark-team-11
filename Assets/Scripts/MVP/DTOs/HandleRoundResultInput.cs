﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class HandleRoundResultInput
    {
        public string RoundId { get; set; }
    }

}
