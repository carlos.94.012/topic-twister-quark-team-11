﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class CheckTurnOutput
    {
        public string[] Input { get; set; }
        public bool[] Checks { get; set; }
        public string TurnId { get; set; }
    }
}