﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class RoundResultOutputDTO
    {
        public string RoundWinnerId { get; set; }
        public string[] TopicsNames { get; set; }
        public int OwnerScore { get; set; }
        public int ChallengerScore { get; set; }
    }
}
