namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class GameSessionHistoricalDTO
    {
            public string WinnerName { get; set; }
            public string OponentName { get; set; }

            public GameSessionHistoricalDTO(string winnerName, string oponentName)
            {
                WinnerName = winnerName;
                OponentName = oponentName;
            }
    }
}