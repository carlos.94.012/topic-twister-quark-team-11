﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class RoundChecker
    {
        public RoundChecker(string roundId, string playerCheckerId, string ownerId, string challengerId)
        {
            RoundId = roundId;
            PlayerCheckerId = playerCheckerId;
            OwnerId = ownerId;
            ChallengerId = challengerId;
        }

        public string RoundId { get; set; }
        public string PlayerCheckerId { get; set; }
        public string OwnerId { get; set; }
        public string ChallengerId { get; set; }
    }
}