﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class AnswerOuput
    {
        public string Input { get; set; }
        public bool IsCorrect { get; set; }
        public string Topic { get; set; }
    }
}