﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class GameSessionActiveDTO
    {
        public string GameSessionId { get; set; }
        public int OpponentScore { get; set; }
        public int PlayerScore { get; set; }
        public int RoundNumber { get; set; }
        public string OpponentName { get; set; }
        public bool WaitForOpponentTurn { get; set; }
        public bool WaitForPlayerTurn { get; set; }
        public bool HasResultCheck { get; set; }
    }
}