﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class GameSessionChecker
    {
        public GameSessionChecker(string gameSessionId, string playerCheckerId)
        {
            GameSessionId = gameSessionId;
            PlayerCheckerId = playerCheckerId;
        }

        public string GameSessionId { get; set; }
        public string PlayerCheckerId { get; set; }
    }
}
