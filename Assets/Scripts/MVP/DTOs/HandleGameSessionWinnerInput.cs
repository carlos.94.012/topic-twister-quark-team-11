﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class HandleGameSessionWinnerInput
    {
        public string GameSessionId { get; set; }
    }

}
