﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class JoinGameSessionOutputDTO
    {
        public string GameSessionID { get; set; }
    }
}