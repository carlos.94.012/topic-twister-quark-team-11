﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class RoundInputDTO
    {
        public int RoundIndex { get; set; }
        public string GamesessionId { get; set; }
    }
}