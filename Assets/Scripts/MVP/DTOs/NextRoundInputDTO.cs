﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class NextRoundInputDTO
    {
        public int RoundIndex { get; set; }
        public string GamesessionId { get; set; }
    }

}
