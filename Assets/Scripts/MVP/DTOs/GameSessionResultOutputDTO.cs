﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class GameSessionResultOutputDTO
    {
        public string PlayerWinnerID { get; set; }
    }
}