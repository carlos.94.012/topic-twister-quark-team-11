﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class TurnOutput
    {
        public string TurnId { get; set; }
    }
}