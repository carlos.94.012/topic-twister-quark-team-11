﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class OpenGameSessionOutputDTO
    {
        public string Id { get; set; }
    }
}