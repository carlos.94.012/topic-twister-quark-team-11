﻿using System.Collections.Generic;

namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class TurnInputDTO
    {
        public string PlayerId { get; set; }
        public string RoundId { get; set; }
        public IEnumerable<AnswerInput> AnswerInputs { get; set; }
    }
}
