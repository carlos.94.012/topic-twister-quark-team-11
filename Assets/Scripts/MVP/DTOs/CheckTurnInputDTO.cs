﻿namespace TopicTwister.DTOs
{
    [System.Serializable]
    public class CheckTurnInputDTO
    {
        public string TurnId { get; set; }
        public string[] Topics { get; set; }
        public string Letter { get; set; }

        public CheckTurnInputDTO(string turnId, string[] topics, string letter)
        {
            TurnId = turnId;
            Topics = topics;
            Letter = letter;
        }
    }
}