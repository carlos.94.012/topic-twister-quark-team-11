using TopicTwister.Delivery;
using TopicTwister.DTOs;
using TopicTwister.Tools;
using TopicTwister.UseCases;
using UnityEngine.SceneManagement;

namespace TopicTwister.Presentation
{
    public class MatchmakingPresenter
    {
        private readonly MatchmakingView _view;

        public MatchmakingPresenter(MatchmakingView view)
        {
            _view = view;
            _view.OnInit += OnInit;
        }

        private async void OnInit()
        {
            var player = SaveSystem.LoadData<PlayerOutputDTO>("/Player");
            _view.SetPlayerName(player.Name);

            var findMatch = new FindGameSession(player.Id);
            var match = await findMatch.Find();
            _view.SetOpponentName(match.OpponentName);

            player.CurrentGameSessionId = match.GameSessionID;

            SaveSystem.SaveData(player, "/Player");

            LeanTween.delayedCall(_view.gameObject, 5f, () => SceneManager.LoadScene("Gameplay") );
        }
    }
}