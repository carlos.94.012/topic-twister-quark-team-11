﻿using TopicTwister.Delivery;
using TopicTwister.UseCases;
using TopicTwister.Tools;
using TopicTwister.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Threading;

namespace TopicTwister.Presentation
{
    public class ActiveGamesListPresenter
    {
        private readonly ActiveGameSessionsListView _view;

        public ActiveGamesListPresenter(ActiveGameSessionsListView view)
        {
            _view = view;
            _view.OnInit += OnInit;
        }

        private async void OnInit()
        {
            var player = SaveSystem.LoadData<PlayerOutputDTO>("/Player");

            var _getActiveGameSessions = new GetActiveGameSessions(player.Id);
            List<GameSessionActiveDTO> data = await _getActiveGameSessions.Get();
                
            foreach (var item in data)
            {
                _view.ShowActiveGame(
                    item.GameSessionId,
                    item.OpponentName,
                    item.RoundNumber,
                    item.WaitForPlayerTurn,
                    item.WaitForOpponentTurn,
                    item.HasResultCheck,
                    item.PlayerScore,
                    item.OpponentScore
                );
            }
        }
    }
}
