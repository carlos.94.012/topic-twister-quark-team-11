using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TopicTwister.Delivery;
using TopicTwister.DTOs;
using TopicTwister.Tools;
using TopicTwister.UseCases;
using UnityEngine;

namespace TopicTwister.Presentation
{
    public class HistoricalGameModalPresenter
    {
        HistoricalGameModalView _view;
        
        public HistoricalGameModalPresenter(HistoricalGameModalView view)
        {
            _view = view;
            _view.OnOpenHistoricalGameModal += OnOpenHistoricalGameModal;
        }

        private async void OnOpenHistoricalGameModal()
        {
            await ShowHistoricals();
        }

        private async Task ShowHistoricals()
        {
            Debug.Log("ShowHistoricals");

            var player = SaveSystem.LoadData<PlayerOutputDTO>("/Player");
            var getFinishedGameSessions = new GetFinishedGameSessions(player.Id);
            GameSessionHistoricalDTO[] items = await getFinishedGameSessions.Get();

            foreach (var item in items)
            {
                _view.ShowHistorical(item.WinnerName, item.OponentName);
            }
        }
    }
}