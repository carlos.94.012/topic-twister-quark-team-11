using System.Threading.Tasks;
using TopicTwister.Delivery;
using TopicTwister.DTOs;
using TopicTwister.Tools;
using TopicTwister.UseCases;
using UnityEngine.SceneManagement;

namespace TopicTwister.Presentation
{
    public class MainMenuScreenPresenter
    {
        private readonly MainMenuScreenView _view;
        public MainMenuScreenPresenter(MainMenuScreenView view)
        {
            _view = view;
            _view.OnPlayButtonClick += OnStartGame;
            _view.AddActionWhenNameIsChanged(ChangeName);
            Init();
        }

        private async Task Init()
        {
            CheckName();
            await ShowPlayerInfo();
            //var player = SaveSystem.LoadData<Player>("/Player");
            //GetFinishedGameSessions getFinishedGameSessions = new GetFinishedGameSessions(player.Id);
            //await getFinishedGameSessions.Get();
            _view.ShowMenu();
        }

        private static bool PlayerNameIsEmpty()
        {
            var player = SaveSystem.LoadData<PlayerOutputDTO>("/Player");
            return string.IsNullOrEmpty(player.Name);
        }

        private async Task ShowPlayerInfo()
        {
            var player = SaveSystem.LoadData<PlayerOutputDTO>("/Player");
            GetPlayer getPlayer = new GetPlayer(player.Id);
            player = await getPlayer.Get();

            _view.ShowPlayerInfo(player.Name, player.Coins);
            _view.ShowActiveGames();
        }

        private void CheckName()
        {
            if (PlayerNameIsEmpty())
                _view.OpenChangeNameModal();
        }

        private void RefreshScreen()
        {
            ShowPlayerInfo();
        }

        private void OnStartGame()
        {
            _view.HideMenu();
        }

        private async void ChangeName(string newName)
        {
            var player = SaveSystem.LoadData<PlayerOutputDTO>("/Player");
            player.Name = newName;
            var task = new ChangePlayerName(player.Id, newName);
            await task.UpdatePlayerName();
            SaveSystem.SaveData(player, "/Player");
            RefreshScreen();
        }
    }
}