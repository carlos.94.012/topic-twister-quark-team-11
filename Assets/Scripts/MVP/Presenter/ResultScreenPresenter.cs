﻿using System.Threading.Tasks;
using TopicTwister.Delivery;
using TopicTwister.DTOs;
using TopicTwister.Tools;
using TopicTwister.UseCases;
using UnityEngine.SceneManagement;

namespace TopicTwister.Presentation
{
    public class ResultScreenPresenter
    {
        private readonly ResultScreenView _resultScreenView;
        private PlayerOutputDTO _player;
        private RoundOutputDTO _round;
        private GameSessionOutputDTO _gameSession;

        public ResultScreenPresenter(ResultScreenView resultScreenView)
        {
            _resultScreenView = resultScreenView;
            _resultScreenView.OnInit += OnInit;
            _resultScreenView.OnNextRoundClick += GoToNextRound;
            _resultScreenView.OnEndGameClick += OnEndGame;
            _resultScreenView.OnPassTheTurnClick += OnPassTheTurn;
            _resultScreenView.OnClaim += OnClaim;
        }

        private async void OnInit()
        {
            _player = SaveSystem.LoadData<PlayerOutputDTO>("/Player");
            _gameSession = await LoadGameSession();
            _round = await LoadRound();

            var roundResult = await LoadRoundResult(_round.Id);

            SetRoundInfo();
            SetScoreboards(roundResult);

            var ownerTurn = await LoadTurn(_gameSession.OwnerId);
            if (ownerTurn != null)
            {
                var ownerAnswer = await LoadAnswers(ownerTurn.TurnId);
                SetAnswers(ownerAnswer, IsOwner(_player.Id));
            }

            var challengerTurn = await LoadTurn(_gameSession.ChallengerId);
            if (challengerTurn != null)
            {
                var challengerAnswers = await LoadAnswers(challengerTurn.TurnId);
                SetAnswers(challengerAnswers, !IsOwner(_player.Id));
            }

            bool hasTurns = ownerTurn != null && challengerTurn != null;

            SetHeaderAndButton(hasTurns);

            _resultScreenView.ShowMenu();
        }

        private Task<AnswerOuput[]> LoadAnswers(string turnId)
        {
            GetAnswersByTurn getAnswersByTurn = new GetAnswersByTurn(turnId);
            var answers = getAnswersByTurn.Get();
            return answers;
        }

        private void ShowGameSessionResult(GameSessionResultOutputDTO gameSessionResult)
        {
            if (IsATie(gameSessionResult))
            {
                _resultScreenView.SetHeaderText("Empate");
                _resultScreenView.ShowEndGameButton();
            }
            else if (HasPlayerWin(gameSessionResult))
            {
                _resultScreenView.SetHeaderText("Has Ganado!");
                _resultScreenView.ShowClaimButton();
            }
            else
            {
                _resultScreenView.SetHeaderText("Haz Perdido");
                _resultScreenView.ShowEndGameButton();
            }
        }

        private bool HasPlayerWin(GameSessionResultOutputDTO gameSessionResult)
        {
            return gameSessionResult?.PlayerWinnerID == _player.Id;
        }

        private bool IsATie(GameSessionResultOutputDTO gameSessionResult)
        {
            return gameSessionResult?.PlayerWinnerID == null;
        }

        private async Task<RoundOutputDTO> LoadRound()
        {
            var getRound = new GetCurrentRoundFromSession(_player.CurrentGameSessionId, _gameSession.RoundIndex, IsOwner(_player.Id));
            return await getRound.Get();
        }

        private async Task<GameSessionOutputDTO> LoadGameSession()
        {
            var getGameSession = new GetGameSession(_player.CurrentGameSessionId);
            return await getGameSession.Get();
        }

        private async Task<RoundResultOutputDTO> LoadRoundResult(string roundId)
        {
            var handleRoundResult = new HandleRoundResult(roundId);
            return await handleRoundResult.Handle();
        }

        private async Task<TurnOutput> LoadTurn(string playerId)
        {
            if (playerId == null) return null;

            var getTurn = new GetTurn(_round.Id, playerId);
            return await getTurn.Get();
        }

        private void SetRoundInfo()
        {
            _resultScreenView.SetRoundIndex(_round.RoundNumber);
            _resultScreenView.SetTopics(_round.TopicNames);
        }

        private void SetScoreboards(RoundResultOutputDTO roundResult)
        {
            if (roundResult != null)
            {
                var ownerScoreBoard = _resultScreenView.GetScoreBoard(IsOwner(_player.Id));
                var opponentScoreBoard = _resultScreenView.GetScoreBoard(IsChallenger(_player.Id));
                _resultScreenView.SetScoreBoard(roundResult.OwnerScore, ownerScoreBoard);
                _resultScreenView.SetScoreBoard(roundResult.ChallengerScore, opponentScoreBoard);
            }
        }

        private bool IsOwner(string playerId) => playerId == _gameSession.OwnerId;
        private bool IsChallenger(string playerId) => playerId == _gameSession.ChallengerId;

        private void SetAnswers(AnswerOuput[] answerOuput, bool playerType)
        {
            if (answerOuput != null)
            {
                for (int i = 0; i < answerOuput.Length; i++)
                {
                    _resultScreenView.SetAnswer(playerType, answerOuput[i].Topic, answerOuput[i].Input, answerOuput[i].IsCorrect);
                }
            }
        }

        private async void SetHeaderAndButton(bool hasTurns)
        {
            if (hasTurns)
            {
                if (_round.IsLastRound)
                {
                    var gameSessionResult = await GetGameSessionWinner();
                    ShowGameSessionResult(gameSessionResult);
                }
                else
                {
                    _resultScreenView.SetHeaderText("Estas por lograrlo");
                    _resultScreenView.ShowNextRoundButton();
                }
            }
            else
            {
                _resultScreenView.SetHeaderText("Finaliza el Turno");
                _resultScreenView.ShowPassTheTurnButton();
            }
        }

        private async Task<GameSessionResultOutputDTO> GetGameSessionWinner()
        {
            if (_round.IsLastRound)
            {
                var getGameSessionWinner = new HandleGameSessionWinner(_player.CurrentGameSessionId);
                return await getGameSessionWinner.Get();
            }

            return null;
        }

        private async void OnEndGame()
        {
            _resultScreenView.EndGame.interactable = false;

            CheckGameSession checkGameSession = new(_gameSession.Id, _player.Id);
            await checkGameSession.Check();
            MoveToScene("MainMenu");
        }

        private async void OnClaim()
        {
            _resultScreenView.Claim.interactable = false;
            var claimGold = new ClaimGold(_player.Id);
            await claimGold.Claim();
            OnEndGame();
        }

        private void OnPassTheTurn()
        {
            _resultScreenView.PassTheTurn.interactable = false;
            MoveToScene("MainMenu");
        }

        public async void GoToNextRound()
        {
            _resultScreenView.NextRound.interactable = false;
            var isOwner = IsOwner(_player.Id);

            if (isOwner && !_round.OwnerResultCheck || !isOwner && !_round.ChallengerResultCheck)
            {
                await new CheckRound(_round.Id, _player.Id, _gameSession.OwnerId, _gameSession.ChallengerId)
                    .Check();
            }

            while (!_round.ChallengerResultCheck || !_round.OwnerResultCheck)
            {
                _round = await new GetRoundForSession(_gameSession.Id, _round.RoundNumber).Get();

                if (_round.ChallengerResultCheck && _round.OwnerResultCheck)
                {
                    break;
                }

                await Task.Delay(System.TimeSpan.FromSeconds(5));
            }

            if (_round.ChallengerResultCheck && _round.OwnerResultCheck)
            {
                if (_gameSession.RoundIndex <= _round.RoundNumber)
                {
                    var setNextRound = new SetNextRound(_round.RoundNumber, _player.CurrentGameSessionId);
                    await setNextRound.Set();
                }
                MoveToScene("Gameplay");
            }
        }

        private void MoveToScene(string scene)
        {
            _resultScreenView.SetHeaderText(string.Empty);
            _resultScreenView.Animator.enabled = false;
            LeanTween.moveY(_resultScreenView.Courtain.gameObject, 0, 1f)
                .setEaseInElastic()
                .setOnComplete(() => LeanTween.delayedCall(1,
                    () =>
                    {
                        SceneManager.LoadScene(scene);
                    }
                )
             );
        }
    }
}