using System.Collections.Generic;
using System.Threading.Tasks;
using TopicTwister.Delivery;
using TopicTwister.DTOs;
using TopicTwister.Tools;
using TopicTwister.UseCases;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TopicTwister.Presentation
{
    public class PlayerAnswersPresenter
    {
        private readonly PlayerAnswersScreenView _view;
        private PlayerOutputDTO _player;
        private RoundOutputDTO _round;

        public PlayerAnswersPresenter(PlayerAnswersScreenView view)
        {
            _view = view;
            _view.OnInit += OnInit;
            _view.OnStopButtonAction += OnFinishTurn;
            _view.OnCountDownFinishAction += OnFinishTurn;
            _view.OnCountDownFinishAction += _view.TurnOffInput;
            _view.OnReturnToMainMenu += OnReturnMainManu;
        }

        private void OnReturnMainManu()
        {
            SceneManager.LoadScene("MainMenu");
        }

        private async void OnInit()
        {
            _player = SaveSystem.LoadData<PlayerOutputDTO>("/Player");

            var getGameSession = new GetGameSession(_player.CurrentGameSessionId);
            var gameSession = await getGameSession.Get();

            var obtainRoundForSession = new ObtainRoundForSession(_player.CurrentGameSessionId, gameSession.RoundIndex);
            _round = await obtainRoundForSession.Obtain();

            SaveSystem.SaveData(_round, "/Round");

            Debug.Log("CurrentGameSessionId: " + _player.CurrentGameSessionId);
            Debug.Log("RoundId: " + _round.Id);

            _view.SetRoundLetter(_round.Letter);
            _view.SetRoundNumber(_round.RoundNumber);
            _view.SetCategories(_round.TopicNames);
            _view.SetMoney(_player.Coins);
            _view.TurnOnInput();
            _view.InitTimer(120f);

            await ListenChallengerTurn(gameSession);
        }

        private async Task ListenChallengerTurn(GameSessionOutputDTO gameSession)
        {
            var opponentId = _player.Id == gameSession.OwnerId ? gameSession.ChallengerId : gameSession.OwnerId;
            var getTurn = new GetTurn(_round.Id, opponentId);
            TurnOutput challengerTurn = null;

            while (challengerTurn == null)
            {
                challengerTurn = await getTurn.Get();

                if (challengerTurn != null)
                {
                    _view.ShowStopScreen();
                    break;
                }

                await Task.Delay(System.TimeSpan.FromSeconds(5));
            }
        }

        private async void OnFinishTurn()
        {
            var answers = _view.GetAnswerInput();

            List<AnswerInput> answerInputs = new List<AnswerInput>();

            for (int i = 0; i < answers.Length; i++)
            {
                var input = answers[i];
                var topic = _round.TopicNames[i];
                answerInputs.Add(new AnswerInput { TopicName = topic, Input = input });
            }

            var sendTurninput = new SendTurnInput(_player.Id, _round.Id, answerInputs);
            var sendTurnOuputDTO = await sendTurninput.Send();
            var CheckTurnAnswers = new CheckTurnAnswers(sendTurnOuputDTO.TurnId, _round.TopicNames, _round.Letter);
            await CheckTurnAnswers.Check();
            SceneManager.LoadScene("Result");
        }
    }
}
 