using System.Threading.Tasks;
using TopicTwister.UseCases;
using TopicTwister.Delivery;
using TopicTwister.Tools;
using UnityEngine;

namespace TopicTwister.Presentation
{
    public class LoadingScreenPresenter
    {
        private readonly LoadingScreenView _view;

        public LoadingScreenPresenter(LoadingScreenView view)
        {
            _view = view;
            _view.OnInit += Init;
        }

        private async void Init()
        {
            await LoginPlayer();
        }

        private async Task LoginPlayer()
        {
            UniqueIdentifierGenerator uniqueIdentifierGenerator = new();
            string uniqueIdentifier = uniqueIdentifierGenerator.Generate();

            Debug.Log(uniqueIdentifier);

            var player = await new GetPlayer(uniqueIdentifier).Get();

            if (player == null)
            {
                player = await new SignUp(uniqueIdentifier).CreatePlayer();
            }

            if (player != null)
            {
                SaveSystem.SaveData(player, "/Player");
            }

            _view.LoadLevel("MainMenu");
        }
    }
}

