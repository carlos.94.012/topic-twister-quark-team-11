using System.Collections.Generic;
using System.Threading.Tasks;
using TopicTwister.GateWay;
using UnityEngine;

namespace TopicTwister.UseCases
{
    public class GetUnfinishedGameSessions
    {
        private readonly HTTPGateway _gateway = new();
        private readonly List<string> _ids;

        public GetUnfinishedGameSessions(List<string> ids)
        {
            _ids = ids;
        }
        
        public async Task UnfinishedGameSessions()
        {
            var response = await _gateway.Post("GameSession/byid", _ids);

            if (!response.IsSuccessStatusCode)
            {
            }

            var result = await response.Content.ReadAsStringAsync();
            Debug.Log(result);
        }
    }
}