﻿using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class CheckRound
    {
        private const string URL = "Round/CheckRound";
        private readonly HTTPGateway _gateway = new();
        private readonly RoundChecker roundChecker;

        public CheckRound(string currentRoundId, string playerCheckerId, string ownerId, string challangerId)
        {
            roundChecker = new(currentRoundId, playerCheckerId, ownerId, challangerId);
        }

        public async Task Check()
        {
            var response = await _gateway.Post(URL, roundChecker);

            if (!response.IsSuccessStatusCode)
            {
                UnityEngine.Debug.LogError($"Error when try to check the round {response.StatusCode} - {response.RequestMessage}");
            }
        }
    }
}