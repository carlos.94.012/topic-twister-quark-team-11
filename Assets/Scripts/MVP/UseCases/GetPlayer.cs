using System.Threading.Tasks;
using Newtonsoft.Json;
using TopicTwister.DTOs;
using TopicTwister.GateWay;
using TopicTwister.Tools;

namespace TopicTwister.UseCases
{
    public class GetPlayer
    {
        private readonly string URL = "Player/GetPlayer";
        private readonly string _id;
        private readonly HTTPGateway _httpGateway = new();

        public GetPlayer(string id)
        {
            _id = id;
        }

        public async Task<PlayerOutputDTO> Get()
        {
            var response = await _httpGateway.Get(
                new EndpointBuilder(URL)
                .Add("id", _id)
                .Build()
            );

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<PlayerOutputDTO>(result);
        }
    }
}