﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class CheckTurnAnswers
    {
        private const string URL = "Turn/CheckTurn";
        private readonly HTTPGateway _gateway = new();
        private readonly CheckTurnInputDTO checkTurnInput;

        public CheckTurnAnswers(string turnId, string[] topicsNames, string letter)
        {
            checkTurnInput = new CheckTurnInputDTO(turnId, topicsNames, letter);
        }

        public async Task<CheckTurnOutput> Check()
        {
            var response = await _gateway.Post(URL, checkTurnInput);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<CheckTurnOutput>(content);
        }
    }
}