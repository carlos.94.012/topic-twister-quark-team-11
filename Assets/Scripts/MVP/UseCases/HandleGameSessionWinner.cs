﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class HandleGameSessionWinner
    {
        private const string URL = "GameSession/HandleGameSessionWinner";
        private readonly HTTPGateway _gateway = new();
        public readonly HandleGameSessionWinnerInput _handleGameSessionWinnerInput;

        public HandleGameSessionWinner(string currentGameSessionId)
        {
            _handleGameSessionWinnerInput = new HandleGameSessionWinnerInput { GameSessionId = currentGameSessionId };
        }

        public async Task<GameSessionResultOutputDTO> Get()
        {
            var response = await _gateway.Post(URL, _handleGameSessionWinnerInput);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<GameSessionResultOutputDTO>(result);
        }
    }
}