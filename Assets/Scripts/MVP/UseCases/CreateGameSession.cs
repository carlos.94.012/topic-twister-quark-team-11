using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class CreateGameSession
    {
        private const string URL = "GameSession/Create";
        private readonly CreateGameSessionInputDTO _createGameSessionInputDTO;
        private readonly HTTPGateway _gateway = new();

        public CreateGameSession(string id)
        {
            _createGameSessionInputDTO = new CreateGameSessionInputDTO
            {
                PlayerId = id
            };
        }

        public async Task<CreateGameSessionOutputDTO> Create()
        {
            var response = await _gateway.Post(URL, _createGameSessionInputDTO);
            
            if (!response.IsSuccessStatusCode)
            {
                UnityEngine.Debug.LogError($"Error when try to create gamesession {response.StatusCode} - {response.RequestMessage}");
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<CreateGameSessionOutputDTO>(result);
        }
    }
}