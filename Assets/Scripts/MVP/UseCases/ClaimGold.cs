﻿using System.Threading.Tasks;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class ClaimGold
    {
        private readonly string _playerId;
        private const string URL = "Player/ClaimGold";
        private readonly HTTPGateway _gateway = new();

        public ClaimGold(string playerId)
        {
            _playerId = playerId;
        }

        public async Task Claim()
        {
            var response = await _gateway.Put(URL, _playerId);

            if (!response.IsSuccessStatusCode)
            {
                UnityEngine.Debug.LogError($"Error when try to claim gold {response.StatusCode} - {response.RequestMessage}");
            }
        }
    }
}