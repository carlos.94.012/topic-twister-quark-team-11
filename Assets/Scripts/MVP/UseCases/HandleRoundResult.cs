﻿using TopicTwister.GateWay;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using Newtonsoft.Json;

namespace TopicTwister.UseCases
{
    public class HandleRoundResult
    {
        private const string URL = "GameSession/HandleRoundResult";
        private readonly HTTPGateway _gateway = new();
        private readonly HandleRoundResultInput _handleRoundResultInput;

        public HandleRoundResult(string roundId)
        {
            _handleRoundResultInput = new HandleRoundResultInput { RoundId = roundId };
        }

        public async Task<RoundResultOutputDTO> Handle()
        {
            var response = await _gateway.Post(URL, _handleRoundResultInput);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<RoundResultOutputDTO>(result);
        }
    }
}
