﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;
using TopicTwister.Tools;

namespace TopicTwister.UseCases
{
    public class GetAnswersByTurn
    {
        private const string URL = "Answer/GetAnswersByTurn";
        private readonly string _turnId;
        private readonly HTTPGateway _gateway = new();

        public GetAnswersByTurn(string turnId)
        {
            _turnId = turnId;
        }

        public async Task<AnswerOuput[]> Get()
        {
            var response = await _gateway.Get(
                new EndpointBuilder(URL)
                    .Add("turnId", _turnId)
                    .Build()
            );

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<AnswerOuput[]>(result);
        }
    }
}