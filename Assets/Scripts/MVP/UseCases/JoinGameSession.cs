using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class JoinGameSession
    {
        private const string URL = "GameSession/Join";
        private readonly JoinGameSessionInputDTO _joinDTO;
        private readonly HTTPGateway _gateway = new();

        public JoinGameSession(string gameSessionID, string playerID)
        {
            _joinDTO = new JoinGameSessionInputDTO()
            {
                GameSessionId = gameSessionID,
                PlayerId = playerID
            };
        }
        
        public async Task<JoinGameSessionOutputDTO> Join()
        {
            var response = await _gateway.Post(URL, _joinDTO);

            if (!response.IsSuccessStatusCode)
            {
                UnityEngine.Debug.LogError($"Error when try join to gamesession {response.StatusCode} - {response.RequestMessage}");
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<JoinGameSessionOutputDTO>(result);
        }
    }
}