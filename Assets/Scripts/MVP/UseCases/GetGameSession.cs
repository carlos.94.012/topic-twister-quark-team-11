﻿using TopicTwister.GateWay;
using TopicTwister.Tools;
using TopicTwister.DTOs;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TopicTwister.UseCases
{
    public class GetGameSession
    {
        private const string URL = "GameSession/ById";
        private readonly string _currentGameSessionId;
        private readonly HTTPGateway _gateway = new();

        public GetGameSession(string currentGameSessionId)
        {
            _currentGameSessionId = currentGameSessionId;
        }

        public async Task<GameSessionOutputDTO> Get()
        {
            var response = await _gateway.Get(
               new EndpointBuilder(URL)
               .Add("id", _currentGameSessionId)
               .Build()
            );

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<GameSessionOutputDTO>(result);
        }
    }
}
