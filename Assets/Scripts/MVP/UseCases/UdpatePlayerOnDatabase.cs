using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class UdpatePlayerOnDatabase
    {
        private readonly HTTPGateway _gateway = new();
        private readonly UpdatePlayerInputDTO _updatePlayerInputDTO;
        private const string url = "Player/update";

        public UdpatePlayerOnDatabase(PlayerOutputDTO player)
        {
            _updatePlayerInputDTO = new UpdatePlayerInputDTO
            {
                PlayerId = player.Id,
                Name = player.Name,
                Coins = player.Coins,
                IsWaitingMatch = player.IsWaitingMatch
            };
        }

        public async Task UpdatePlayer()
        {
            var response = await _gateway.Post(url, _updatePlayerInputDTO);

            if (!response.IsSuccessStatusCode)
            {
                UnityEngine.Debug.LogError($"Error when try to update player {response.StatusCode} - {response.RequestMessage}");
            }

        }
    }
}