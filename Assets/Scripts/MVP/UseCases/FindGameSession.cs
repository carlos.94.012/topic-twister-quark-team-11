using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;
using TopicTwister.Tools;

namespace TopicTwister.UseCases
{
    public class FindGameSession
    {
        private readonly string URL = "Gamesession/FindMatch";
        private readonly string _playerId;
        private readonly HTTPGateway gateway = new();

        public FindGameSession(string playerId)
        {
            _playerId = playerId;
        }

        public async Task<CreateGameSessionOutputDTO> Find()
        {
            GetPlayer getPlayer = new GetPlayer(_playerId);
            var player = await getPlayer.Get();
            player.IsWaitingMatch = true;

            UdpatePlayerOnDatabase udpatePlayerOnDatabase = new UdpatePlayerOnDatabase(player);
            await udpatePlayerOnDatabase.UpdatePlayer();

            var endpoint = new EndpointBuilder(URL).Add("playerId", _playerId).Build();
            var response = await gateway.Get(endpoint);

            while (!response.IsSuccessStatusCode)
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                response = await gateway.Get(endpoint);
            }

            var result = await response.Content.ReadAsStringAsync();
            var output = JsonConvert.DeserializeObject<CreateGameSessionOutputDTO>(result);
            return output;
        }
    }

    // public class CheckForGameSessionOpen
    // {
    //     private readonly string URL = "Gamesession/FirstOpen";
    //     private readonly string _playerId;
    //     private readonly HTTPGateway gateway = new();

    //     public CheckForGameSessionOpen(string playerId)
    //     {
    //         _playerId = playerId;
    //     }

    //     public async Task<OpenGameSessionOutputDTO> Get()
    //     {
    //         var endpoint = new EndpointBuilder(URL).Add("playerId", _playerId).Build();
    //         var response = await gateway.Get(endpoint);
            
    //         if (response.StatusCode != HttpStatusCode.OK)
    //         {
    //             return null;
    //         }
  
    //         var result = await response.Content.ReadAsStringAsync();
    //         var dto = JsonConvert.DeserializeObject<OpenGameSessionOutputDTO>(result);
    //         return dto;
    //     }
    // }
}