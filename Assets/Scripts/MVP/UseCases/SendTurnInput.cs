﻿using TopicTwister.GateWay;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace TopicTwister.UseCases
{
    public class SendTurnInput
    {
        private readonly TurnInputDTO _turnInputDTO;
        private readonly HTTPGateway _gateway = new();
        private const string URL = "Turn/SendTurnInput";

        public SendTurnInput(string playerId, string roundId, IEnumerable<AnswerInput> answerInputs)
        {
            
            _turnInputDTO = new TurnInputDTO
            {
                PlayerId = playerId,
                RoundId = roundId,
                AnswerInputs = answerInputs
            };
        }

        public async Task<SendTurnOuputDTO> Send()
        {
            var response = await _gateway.Post(URL, _turnInputDTO);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<SendTurnOuputDTO>(json);
        }
    }
}
