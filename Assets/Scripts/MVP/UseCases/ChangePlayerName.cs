using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class ChangePlayerName
    {
        private const string URL = "Player/UpdatePlayerName";
        private readonly UpdatePlayerNameInputDTO _updatePlayerNameDto;
        private readonly HTTPGateway _gateway = new();

        public ChangePlayerName(string id, string newName)
        {
            _updatePlayerNameDto = new UpdatePlayerNameInputDTO
            {
                PlayerId = id,
                Name = newName.ToUpper()
            };
        }

        public async Task UpdatePlayerName()
        {
            var response = await _gateway.Put(URL, _updatePlayerNameDto);
            
            if (!response.IsSuccessStatusCode)
            {
                UnityEngine.Debug.LogError($"Error when try to update player name {response.StatusCode} - {response.RequestMessage}");       
            }
        }
    }
}