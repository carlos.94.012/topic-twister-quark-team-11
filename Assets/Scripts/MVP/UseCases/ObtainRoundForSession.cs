﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class ObtainRoundForSession
    {
        private readonly string _url = "Round/ObtainRoundForSession";
        private readonly HTTPGateway _gateway = new();
        private readonly RoundInputDTO _roundInputDTO;

        public ObtainRoundForSession(string gameSessionId, int roundIndex)
        {
            _roundInputDTO = new RoundInputDTO
            {
                GamesessionId = gameSessionId,
                RoundIndex = roundIndex
            };
        }

        public async Task<RoundOutputDTO> Obtain()
        {
            var response = await _gateway.Post(_url, _roundInputDTO);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<RoundOutputDTO>(result);
        }
    }

    public class GetRoundForSession
    {
        private readonly string _url = "Round/GetRoundForSession";
        private readonly HTTPGateway _gateway = new();
        private readonly RoundInputDTO _roundInputDTO;

        public GetRoundForSession(string gameSessionId, int roundIndex)
        {
            _roundInputDTO = new RoundInputDTO
            {
                GamesessionId = gameSessionId,
                RoundIndex = roundIndex
            };
        }

        public async Task<RoundOutputDTO> Get()
        {
            var response = await _gateway.Post(_url, _roundInputDTO);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<RoundOutputDTO>(result);
        }
    }
}