﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class SignUp
    {
        private readonly string _id;
        private const string URL = "Player/CreatePlayer";
        private readonly HTTPGateway _httpGateway = new();
        private readonly PlayerInputDTO playerInputDTO;

        public SignUp(string id)
        {
            playerInputDTO = new PlayerInputDTO { Id = id };
        }

        public async Task<PlayerOutputDTO> CreatePlayer()
        {
            var response = await _httpGateway.Post(URL, playerInputDTO);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<PlayerOutputDTO>(result);
        }
    }
}