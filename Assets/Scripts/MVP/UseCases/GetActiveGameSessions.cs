﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;
using TopicTwister.Tools;

namespace TopicTwister.UseCases
{
    public class GetActiveGameSessions
    {
        public readonly HTTPGateway _gateway = new();
        public const string URL = "GameSession/GetActives";
        public readonly string _playerId;

        public GetActiveGameSessions(string playerId)
        {
            _playerId = playerId;
        }

        public async Task<List<GameSessionActiveDTO>> Get()
        {
            var response = await _gateway.Get(new EndpointBuilder(URL)
               .Add("playerId", _playerId)
               .Build()
            );

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<GameSessionActiveDTO>>(result);
        }
    }
}
