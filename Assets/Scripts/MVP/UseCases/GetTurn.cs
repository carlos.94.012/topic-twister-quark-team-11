﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.GateWay;
using TopicTwister.Tools;

namespace TopicTwister.DTOs
{
    public class GetTurn
    {
        private const string URL = "Turn/GetTurn";
        private readonly string _roundId;
        private readonly string _playerId;
        private readonly HTTPGateway _gateway = new();

        public GetTurn(string turnId, string playerId)
        {
            _roundId = turnId;
            _playerId = playerId;
        }

        public async Task<TurnOutput> Get()
        {
            var response = await _gateway.Get(
                new EndpointBuilder(URL)
                    .Add("roundId", _roundId)
                    .Add("playerId", _playerId)
                    .Build()
            );

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject<TurnOutput>(result);
            return json;
        }
    }
}