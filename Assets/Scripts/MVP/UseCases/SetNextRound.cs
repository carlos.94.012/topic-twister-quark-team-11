﻿using TopicTwister.GateWay;
using TopicTwister.DTOs;
using System.Threading.Tasks;

namespace TopicTwister.UseCases
{
    public class SetNextRound
    {
        private const string Url = "GameSession/SetNextRound";
        private readonly HTTPGateway _gateway = new();
        private readonly NextRoundInputDTO _nextRoundInputDTO;

        public SetNextRound(int roundIndex, string gamesessionId)
        {
            _nextRoundInputDTO = new()
            {
                GamesessionId = gamesessionId,
                RoundIndex = roundIndex
            };
        }

        public async Task Set()
        {
            var response = await _gateway.Post(Url, _nextRoundInputDTO);

            if (!response.IsSuccessStatusCode)
            {
                UnityEngine.Debug.LogError($"Error when try to set next round {response.StatusCode} - {response.RequestMessage}");
            }
        }
    }
}

