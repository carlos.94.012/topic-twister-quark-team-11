using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;
using TopicTwister.Tools;

namespace TopicTwister.UseCases
{
    public class GetFinishedGameSessions
    {
        private const string URL = "GameSession/GetFinished";
        private readonly string _playerId;
        private readonly HTTPGateway _gateway = new();

        public GetFinishedGameSessions(string playerId)
        {
            _playerId = playerId;
        }

        public async Task<GameSessionHistoricalDTO[]> Get()
        {
            var response = await _gateway.Get(
                new EndpointBuilder(URL)
                .Add("playerId", _playerId)
                .Build()
            );

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<GameSessionHistoricalDTO[]>(result);
        }
    }
}