﻿using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;

namespace TopicTwister.UseCases
{
    public class CheckGameSession
    {
        private const string URL = "GameSession/CheckGameSession";
        private readonly HTTPGateway _gateway = new();
        private readonly GameSessionChecker gameSessionChecker;

        public CheckGameSession(string currentGameSessionId, string playerCheckerId)
        {
            gameSessionChecker = new(currentGameSessionId, playerCheckerId);
        }

        public async Task Check()
        {
            var response = await _gateway.Post(URL, gameSessionChecker);

            if (!response.IsSuccessStatusCode)
            {
                UnityEngine.Debug.LogError("Error To check gamesession:  " + response.StatusCode + response.RequestMessage);
            }
        }
    }
}