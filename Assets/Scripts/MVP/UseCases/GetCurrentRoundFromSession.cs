using Newtonsoft.Json;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.GateWay;
using TopicTwister.Tools;

namespace TopicTwister.UseCases
{
    public class GetCurrentRoundFromSession
    {
        private readonly string _url = "Round/GetCurrentRound";
        private readonly string _gameSessionId;
        public readonly int _roundIndex;
        public readonly bool _isOwner;
        private readonly HTTPGateway _gateway = new ();

        public GetCurrentRoundFromSession(string gameSessionId, int roundIndex, bool isOwner)
        {
            _gameSessionId = gameSessionId;
            _roundIndex = roundIndex;
            _isOwner = isOwner;
        }

        public async Task<RoundOutputDTO> Get()
        {
            var response = await _gateway.Get(
                new EndpointBuilder(_url)
                .Add("gamesessionId", _gameSessionId)
                .Add("roundIndex", _roundIndex.ToString())
                .Add("isOwner", _isOwner.ToString())
                .Build()
            );

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<RoundOutputDTO>(result);
        }
    }
}