﻿using System;
using UnityEngine;

namespace TopicTwister.Tools
{
    public class UniqueIdentifierGenerator
    {
        public string Generate()
        {
            string identifierKey = Application.isEditor ? "EditorId" : "UniqueId";
            string uniqueIdentifier = PlayerPrefs.GetString(identifierKey);

            if (string.IsNullOrEmpty(uniqueIdentifier))
            {
                uniqueIdentifier = Guid.NewGuid().ToString();
                PlayerPrefs.SetString(identifierKey, uniqueIdentifier);
                PlayerPrefs.Save();
            }

            return uniqueIdentifier;
        }
    }
}
