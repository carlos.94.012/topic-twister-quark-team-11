using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace TopicTwister.Tools
{
    public static class SaveSystem
    {
        public static void SaveData<T>(T save,string endPoint)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = (Application.isEditor ? Application.persistentDataPath : Application.dataPath);
            path += endPoint + ".fun";
            var stream = new FileStream(path, FileMode.Create);
            formatter.Serialize(stream, save);
            stream.Close();
        }

        public static T LoadData<T>(string endPoint)
        {
            string path = (Application.isEditor ? Application.persistentDataPath : Application.dataPath);
            path += endPoint + ".fun";

            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                Debug.Log(path);
                FileStream stream = new FileStream(path, FileMode.Open);
                T data = (T) formatter.Deserialize(stream);
                stream.Close();
                return data;
            }
            return default;
        }
    }
}