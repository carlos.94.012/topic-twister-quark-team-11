﻿using System.Collections;
using System.Collections.Generic;
using TopicTwister.GateWay;
using System.Threading.Tasks;
using TopicTwister.DTOs;
using TopicTwister.Tools;
using System.Linq;

namespace TopicTwister.Tools
{
    public class EndpointBuilder
    {
        private Dictionary<string, string> _query = new();
        private readonly string _url;

        public EndpointBuilder(string url)
        {
            _url = url;
        }

        public EndpointBuilder Add(string key, string value)
        {
            _query.Add(key, value);
            return this;
        }

        public string Build()
        {
            var queryString = string.Join('&', _query.Select(q => $"{q.Key}={q.Value}"));
            return $"{_url}?{queryString}";
        }
    }
}
