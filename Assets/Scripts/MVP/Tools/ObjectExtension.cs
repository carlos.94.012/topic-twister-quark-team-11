using System.Net.Http;
using Newtonsoft.Json;

namespace TopicTwister.Tools
{
    public static class ObjectExtension
    {
        public static StringContent ToStringContentJSON<T>(this T variable)
        {
            string json = JsonConvert.SerializeObject(variable);
            return new StringContent(json, System.Text.Encoding.UTF8, "application/json");
        }
    }
}