using UnityEditor;
using UnityEditor.SceneManagement;

namespace EditorTools
{
    public class SetFirstScene
    {
        const string STARTUP_SCENE = "Assets/Scenes/LoadingScreenInit.unity";

        [InitializeOnLoadMethod]
        public static void OnEnterPlayGame()
        {
            var myScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(STARTUP_SCENE);
            //EditorSceneManager.playModeStartScene = myScene;
        }
    }
}