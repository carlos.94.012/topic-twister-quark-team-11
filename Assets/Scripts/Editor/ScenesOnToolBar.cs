#define UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

namespace EditorTools
{
    public class ScenesOnToolBar
    {
        const string LOADING_SCREEN_SCENE = "Assets/Scenes/LoadingScreenInit.unity";
        const string GAME_SCENE = "Assets/Scenes/MainMenu.unity";
        const string GAME_OVER = "Assets/Scenes/GameOver.unity";
        const string RESULT = "Assets/Scenes/Result.unity";
        const string MATCHMAKING = "Assets/Scenes/MatchMaking.unity";
        const string GAMEPLAY = "Assets/Scenes/Gameplay.unity";


        [MenuItem("Scenes/Loading")]
        public static void GoToLoadingScene()
        {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                EditorSceneManager.OpenScene(LOADING_SCREEN_SCENE);
        }

        [MenuItem("Scenes/MainMenu")]
        public static void GoToMainMenuScene()
        {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                EditorSceneManager.OpenScene(GAME_SCENE);
        }

        [MenuItem("Scenes/MatchMaking")]
        public static void GoToMatchMakingScene()
        {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                EditorSceneManager.OpenScene(MATCHMAKING);
        }

        [MenuItem("Scenes/Gameplay")]
        public static void GoToGamePlayScene()
        {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                EditorSceneManager.OpenScene(GAMEPLAY);
        }

        [MenuItem("Scenes/Results")]
        public static void GoToResultsScene()
        {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                EditorSceneManager.OpenScene(RESULT);
        }

        [MenuItem("Scenes/GameOver")]
        public static void GoToGameOverScene()
        {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                EditorSceneManager.OpenScene(GAME_OVER);
        }
    }
}