﻿using System;
using System.Threading.Tasks;

namespace TopicTwister.UseCases
{
    internal class GetPlayerTurnId
    {
        private readonly string _playerId;
        private readonly string _roundId;

        public GetPlayerTurnId(string playerId, string roundId)
        {
            _playerId = playerId;
            _roundId = roundId;
        }

        public async Task<string> Execute()
        {
            throw new NotImplementedException();
        }
    }
}